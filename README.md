# cnMANO

## 介绍
基于springboot和k8s-client开发，通过访问k8s api-server监控核心网信息，完成5G核心网的编排与管理

## 软件架构
### 依赖关系
![输入图片说明](https://images.gitee.com/uploads/images/2021/0529/174152_c03bebdf_8106842.png "屏幕截图.png")

所有微服务都依赖api-common,api-common中包含所有工程所依赖的:
1. java bean：数据库对象都在entities包下面，网页参数对象在requestbody下面
2. 工具类：utils包下面，主要用于微服务之间发起restful http请求以及系统调用（monitor有自己单独用的工具类，没有放到api-common里面）

### 每个工程的一般架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0529/174305_b3e1dd6c_8106842.png "屏幕截图.png")

其中
1. Controller层负责处理HTTP请求，如果需要调用本工程中的代码，则调用工程内的Service，如果需要远程请求，则使用api-common里面的工具类发起http请求
2. Service层负责主要的业务处理，基本所有的流程都在Service的方法中实现
3. Dao（Data Access Object）层负责与数据库的交互，基于Mybatis，每个dao包下面的mapper（interface）都对应resources/mapper文件夹下的一个mapper（xml），这些文件都是通过mybatis-generator根据数据库中的表自动生成的，支持数据表的简单的增删改查

## 安装教程

### 环境需求：

jdk 8，maven 3.6.2，kubernetes 1.17.0，mysql 5.7
### 前置需求

首先需要K8S的api-server开放HTTP接口，一般情况下api-server开放的是HTTPS访问，这里需要开放HTTP协议的API，使得资源指标监控服务可以监控K8S集群的状况，教程可以参考https://cooting.cn/archives/51.html

### 安装步骤
当前的工作目录用$WORKDIR代表，实际操作的时候需要替换成你的工作目录

1. 首先clone代码

```
git clone https://gitee.com/judabai/cn-mano.git
```

2. 之后导入数据库

先mysql -uroot -p登入本地mysql控制台，在mysql控制台中操作
```
create database cnmano;
use cnmano;
source $WORKDIR/resources/dbback/cnmano.txt;
```

3.工程打包

在$WORKDIR下操作
```
mvn package
```
第一次可能需要比较长的时间，需要下载较多的依赖。

之后执行
```
bash $WORKDIR/resources/scripts/copyjars.sh
```
把所有的jar包汇总到resources/jars下面

## 使用说明
在$WORKDIR/resources/jars下，依次
```
java -jar xxx.jar
```
将每一个微服务运行起来即可

运行起来之后，访问localhost:8000/nsd可以访问到NSD的管理界面，之后可以通过侧边栏访问各个微服务

### NSD的创建
NSD包含对多个表的引用，一个NSD的创建需要从底层配置（VNFDF，VduProfile等等）开始，最后汇聚再创建NSD，具体的每个配置所包含的信息请查看cnMANO需求文档.pdf
### 镜像上传
镜像通过Harbor命令行上传
同时镜像服务中需要手动更新镜像信息，需要与Harbor中一致
### 核心网实例的创建
见论文第三章
### 核心网实例的实例化
见论文第三章
### 核心网实例的实例的扩缩容
见论文第三章
### 核心网实例的终止
见论文第三章
### 核心网实例的删除
见论文第三章