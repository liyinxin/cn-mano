package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;
import edu.xidian.cnmano.service.ImageManagementService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-14:59
 */
@RequestMapping("/images")
@RestController
public class ImageManagementController {
    @Resource
    private ImageManagementService imageManagementService;

    @RequestMapping("")
    public List<Swimagedesc> listImage(){
        return imageManagementService.listImage();
    }

    @RequestMapping("/{id}")
    public Swimagedesc getSwimageById(@PathVariable("id") Integer id){
        return imageManagementService.getSwimageById(id);
    }

    @RequestMapping("/create")
    public int createImage(Swimagedesc swimagedesc){
        return imageManagementService.createImage(swimagedesc);
    }

    @RequestMapping("/delete")
    public int deleteImage(int id){
        return imageManagementService.deleteImage(id);
    }
}
