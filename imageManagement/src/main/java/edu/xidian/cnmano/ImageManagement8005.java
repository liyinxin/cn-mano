package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2021/1/14-14:57
 */
@SpringBootApplication
public class ImageManagement8005 {
    public static void main(String[] args) {
        SpringApplication.run(ImageManagement8005.class,args);
    }
}
