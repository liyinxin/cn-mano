package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.SwimagedescMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;
import edu.xidian.cnmano.entities.nsdmanagement.SwimagedescExample;
import edu.xidian.cnmano.service.ImageManagementService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-15:00
 */
@Service
public class ImageManagementServiceImpl implements ImageManagementService {
    @Resource
    private SwimagedescMapper mapper;

    @Override
    public List<Swimagedesc> listImage() {
        SwimagedescExample example = new SwimagedescExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createImage(Swimagedesc swimagedesc) {
        return mapper.insert(swimagedesc);
    }

    @Override
    public int deleteImage(int id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public Swimagedesc getSwimageById(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }
}
