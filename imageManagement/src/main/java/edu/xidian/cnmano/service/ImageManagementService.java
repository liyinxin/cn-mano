package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-14:59
 */
public interface ImageManagementService {
    List<Swimagedesc> listImage();

    int createImage(Swimagedesc swimagedesc);

    int deleteImage(int id);

    Swimagedesc getSwimageById(Integer id);
}
