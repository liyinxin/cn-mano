package edu.xidian.cnmano.cronjob;

import edu.xidian.cnmano.entities.monitor.PodInfo;
import edu.xidian.cnmano.service.PodInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**定时任务，负责定时访问metrics-server，更新podInfo表中资源占用相关的信息
 * @author zhr
 * @date 2020/11/17-17:49
 */
@Component
@Slf4j
public class PodInfoCronJob {

    @Resource
    private PodInfoService podInfoService;

    @Scheduled(cron = "0/10 * * * * ?")
    public void updateOrInsertPodInfo(){
        List<PodInfo> podInfoFromCluster = podInfoService.getPodMetricsInfoFromCluster();
        //log.info("podInfo update/insert cronjob running");
        for(PodInfo podInfo : podInfoFromCluster){
            PodInfo podInfoByPodName = podInfoService.getPodInfoByPodNameAndNamespace(podInfo.getPodName(),podInfo.getNamespace());
            if(podInfoByPodName!=null){
                podInfoService.updatePodInfo(podInfo);
            }else{
                podInfoService.insertPodInfo(podInfo);
            }
        }
    }
}
