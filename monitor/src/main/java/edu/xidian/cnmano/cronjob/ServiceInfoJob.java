package edu.xidian.cnmano.cronjob;

import edu.xidian.cnmano.entities.monitor.ServiceInfo;
import edu.xidian.cnmano.service.ServiceInfoService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/17-14:39
 */
@Component
public class ServiceInfoJob {

    @Resource
    private ServiceInfoService serviceInfoService;

    /**
     * 查询service的任务只需要执行一次就可以了，之后依靠informer来更新serviceInfo的信息
     */
    @PostConstruct
    public void updateOrInsertServiceInfo(){
        List<ServiceInfo> serviceInfoList = serviceInfoService.listServiceInfoFromCluster();
        for(ServiceInfo serviceInfo:serviceInfoList){
            if (serviceInfoService.getServiceInfoByServiceNameAndNamespace(serviceInfo.getServiceName(),serviceInfo.getNamespace())!=null){
                serviceInfoService.updateServiceInfo(serviceInfo);
            }else{
                serviceInfoService.insertServiceInfo(serviceInfo);
            }
        }
    }
}
