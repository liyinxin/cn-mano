package edu.xidian.cnmano.cronjob;

import edu.xidian.cnmano.entities.monitor.NodeInfo;
import edu.xidian.cnmano.service.NodeInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**定时任务，负责定时访问k8s集群，更新nodeInfo表中cpu和内存占用状况的信息(Informer只能统计总的信息，使用量需要访问metrics-server
 * 才可以访问到)
 * @author zhr
 * @date 2020/11/17-17:50
 */
@Component
@Slf4j
public class NodeInfoCronJob {

    @Resource
    private NodeInfoService nodeInfoService;

    @Scheduled(cron = "0/10 * * * * ?")
    public void updateOrInsertNodeInfo() {
        //除了cpu和内存占用之外，节点信息一般不会变化，所以只需要定时更新资源信息
        //log.info("nodeInfo update/insert cronjob running");
        List<NodeInfo> nodeMetricsInfoList = nodeInfoService.listNodeMetricsInfoFromCluster();
        for (NodeInfo nodeInfo : nodeMetricsInfoList) {
            NodeInfo nodeInfoByNodeName = nodeInfoService.getNodeInfoByNodeName(nodeInfo.getNodeName());
            /*if (nodeInfo.getUsedCpu()*1.0/nodeInfoByNodeName.getTotalCpu() >= 0.8) {
                nodeInfo.setCpuPressure(1);
            }else{
                nodeInfo.setCpuPressure(0);
            }*/
            nodeInfoService.updateNodeInfo(nodeInfo);
        }
    }
}
