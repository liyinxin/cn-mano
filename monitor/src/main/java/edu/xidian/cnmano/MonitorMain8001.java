package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class MonitorMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(MonitorMain8001.class,args);
    }
}
