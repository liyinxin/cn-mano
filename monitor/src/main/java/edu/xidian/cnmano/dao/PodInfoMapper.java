package edu.xidian.cnmano.dao;

import edu.xidian.cnmano.entities.monitor.PodInfo;
import edu.xidian.cnmano.entities.monitor.PodInfoExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PodInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    long countByExample(PodInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int deleteByExample(PodInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int insert(PodInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int insertSelective(PodInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    List<PodInfo> selectByExample(PodInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    PodInfo selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int updateByExampleSelective(@Param("record") PodInfo record, @Param("example") PodInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int updateByExample(@Param("record") PodInfo record, @Param("example") PodInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int updateByPrimaryKeySelective(PodInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table pod_info
     *
     * @mbg.generated Tue Jan 19 22:51:46 CST 2021
     */
    int updateByPrimaryKey(PodInfo record);
}