package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.monitor.DeploymentInfo;
import edu.xidian.cnmano.service.DeploymentInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 主要负责处理访问DeploymentInfo的相关http请求
 * @author zhr 2020/11/17
 */
@Slf4j
@RequestMapping("/deploymentInfo")
@RestController
public class DeploymentInfoController {

    @Resource
    private DeploymentInfoService deployementInfoService;

    @RequestMapping("/{id}")
    public DeploymentInfo getDeploymentInfoById(@PathVariable("id")int id){
        return deployementInfoService.getDeploymentInfoById(id);
    }

    @RequestMapping("/{namespace}/{deploymentName}")
    public DeploymentInfo getDeploymentInfoByDeploymentName(@PathVariable("namespace") String namespace,@PathVariable("deploymentName")String deploymentName){
        return deployementInfoService.getDeploymentInfoByDeploymentNameAndNamespace(deploymentName,namespace);
    }

    @RequestMapping("")
    public List<DeploymentInfo> getAllDeploymentInfo(){
        return deployementInfoService.listAllDeploymentInfo();
    }
}
