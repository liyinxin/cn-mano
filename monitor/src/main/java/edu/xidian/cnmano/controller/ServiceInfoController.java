package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.monitor.ServiceInfo;
import edu.xidian.cnmano.service.ServiceInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/17-10:59
 */
@RestController
@RequestMapping("/serviceInfo")
public class ServiceInfoController {
    @Resource
    ServiceInfoService serviceInfoService;

    @RequestMapping("")
    public List<ServiceInfo> listServiceInfo(){
        return serviceInfoService.listServiceInfo();
    }
}
