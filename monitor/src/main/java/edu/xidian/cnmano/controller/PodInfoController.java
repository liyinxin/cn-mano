package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.monitor.PodInfo;
import edu.xidian.cnmano.service.PodInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 主要负责处理访问PodInfo的相关http请求
 * @author zhr 2020/11/17
 */
@Slf4j
@RestController
@RequestMapping("/podInfo")
public class PodInfoController {

    @Resource
    private PodInfoService podInfoService;

    @RequestMapping("/{id}")
    public PodInfo getPodInfoById(@PathVariable("id")int id){
        return podInfoService.getPodInfoById(id);
    }

    @RequestMapping("/{namespace}/{podName}")
    public PodInfo getPodInfoByPodName(@PathVariable("namespace") String namespace,@PathVariable("podName")String podName){
        return podInfoService.getPodInfoByPodNameAndNamespace(podName,namespace);
    }

    @RequestMapping("")
    public List<PodInfo> getAllPodInfo(Model model){
        return podInfoService.listAllPodInfo();
    }
}
