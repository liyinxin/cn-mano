package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.monitor.NodeInfo;
import edu.xidian.cnmano.service.NodeInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * 主要负责处理访问NodeInfo的相关http请求
 * @author zhr 2020/11/17
 */
@Slf4j
@RestController
@RequestMapping("/nodeInfo")
public class NodeInfoController {

    @Resource
    private NodeInfoService nodeInfoService;

    @RequestMapping("/{id}")
    public NodeInfo getNodeInfoById(@PathVariable("id") int id){
        return nodeInfoService.getNodeInfoById(id);
    }

    @RequestMapping("/{nodeName}")
    public NodeInfo getNodeInfoByNodeName(@PathVariable("nodeName") String nodeName){
        return nodeInfoService.getNodeInfoByNodeName(nodeName);
    }

    @RequestMapping("")
    public List<NodeInfo> getAllNodeInfo(){
        return nodeInfoService.listAllNodeInfo();
    }
}
