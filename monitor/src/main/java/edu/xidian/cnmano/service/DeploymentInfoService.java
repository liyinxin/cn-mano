package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.monitor.DeploymentInfo;

import java.util.List;

public interface DeploymentInfoService {
    DeploymentInfo getDeploymentInfoById(int id);

    DeploymentInfo getDeploymentInfoByDeploymentNameAndNamespace(String deploymentName,String namespace);

    List<DeploymentInfo> listAllDeploymentInfo();

    int updateDeploymentInfo(DeploymentInfo deploymentInfo);

    int insertDeploymentInfo(DeploymentInfo deploymentInfo);

    int deleteDeploymentInfo(DeploymentInfo deploymentInfo);
}
