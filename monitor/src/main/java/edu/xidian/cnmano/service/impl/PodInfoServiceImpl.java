package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.api.MetricsServerApi;
import edu.xidian.cnmano.dao.PodInfoMapper;
import edu.xidian.cnmano.entities.monitor.MetricsPod;
import edu.xidian.cnmano.entities.monitor.MetricsPodList;
import edu.xidian.cnmano.entities.monitor.PodInfo;
import edu.xidian.cnmano.entities.monitor.PodInfoExample;
import edu.xidian.cnmano.service.PodInfoService;
import edu.xidian.cnmano.util.K8sElementInfoUtil;
import io.kubernetes.client.openapi.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Pod相关的Service,主要负责PodInfo相关的增删改查
 */
@Slf4j
@Service
public class PodInfoServiceImpl implements PodInfoService {

    @Resource
    private PodInfoMapper mapper;

    @Resource
    private MetricsServerApi metricsServerApi;

    @Override
    public PodInfo getPodInfoById(int id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public PodInfo getPodInfoByPodNameAndNamespace(String podName, String namespace) {
        PodInfoExample example = new PodInfoExample();
        example.createCriteria().andPodNameEqualTo(podName).andNamespaceEqualTo(namespace);
        List<PodInfo> podInfos = mapper.selectByExample(example);
        if (podInfos == null || podInfos.isEmpty()) {
            return null;
        } else {
            return podInfos.get(0);
        }
    }

    @Override
    public List<PodInfo> listPodInfoByDeploymentNameAndNamespace(String deploymentName,String namespace) {
        PodInfoExample example = new PodInfoExample();
        example.createCriteria().andDeploymentNameEqualTo(deploymentName).andNamespaceEqualTo(namespace);
        return mapper.selectByExample(example);
    }

    @Override
    public List<PodInfo> listAllPodInfo() {
        PodInfoExample example = new PodInfoExample();
        List<PodInfo> podInfos = mapper.selectByExample(example);
        podInfos.sort(Comparator.comparing(PodInfo::getNamespace));
        return podInfos;
    }

    @Override
    public int updatePodInfo(PodInfo podInfo) {
        try {
            PodInfoExample example = new PodInfoExample();
            example.createCriteria().andPodNameEqualTo(podInfo.getPodName()).andNamespaceEqualTo(podInfo.getNamespace());
            return mapper.updateByExampleSelective(podInfo, example);
        } catch (Exception e) {
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public int insertPodInfo(PodInfo podInfo) {
        try {
            return mapper.insert(podInfo);
        } catch (Exception e) {
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public List<PodInfo> getPodMetricsInfoFromCluster() {
        List<PodInfo> podInfoList = new ArrayList<>();
        try {
            MetricsPodList metricsPodList = metricsServerApi.listPodForAllNamespaces();
            for (MetricsPod metricsPod : metricsPodList.getItems()) {
                podInfoList.add(K8sElementInfoUtil.MetricsPodToPodInfo(metricsPod));
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return podInfoList;
    }

    @Override
    public int deletePodInfo(PodInfo podInfo) {
        try {
            PodInfoExample example = new PodInfoExample();
            example.createCriteria().andPodNameEqualTo(podInfo.getPodName()).andNamespaceEqualTo(podInfo.getNamespace());
            return mapper.deleteByExample(example);
        } catch (Exception e) {
            log.error(e.getMessage());
            return 0;
        }
    }


}
