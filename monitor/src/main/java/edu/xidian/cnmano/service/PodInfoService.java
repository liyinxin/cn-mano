package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.monitor.PodInfo;

import java.util.List;

public interface PodInfoService {
    public PodInfo getPodInfoById(int id);

    public PodInfo getPodInfoByPodNameAndNamespace(String podName,String namespace);

    public List<PodInfo> listPodInfoByDeploymentNameAndNamespace(String deploymentName,String namespace);

    public List<PodInfo> listAllPodInfo();

    public int updatePodInfo(PodInfo podInfo);

    public int insertPodInfo(PodInfo podInfo);

    public List<PodInfo> getPodMetricsInfoFromCluster();

    public int deletePodInfo(PodInfo podInfo);
}

