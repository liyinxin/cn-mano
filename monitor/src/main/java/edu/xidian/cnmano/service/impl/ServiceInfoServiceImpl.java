package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.ServiceInfoMapper;
import edu.xidian.cnmano.entities.monitor.ServiceInfo;
import edu.xidian.cnmano.entities.monitor.ServiceInfoExample;
import edu.xidian.cnmano.service.ServiceInfoService;
import edu.xidian.cnmano.util.K8sElementInfoUtil;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.openapi.models.V1ServiceList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/17-11:00
 */
@Service
@Slf4j
public class ServiceInfoServiceImpl implements ServiceInfoService {

    @Resource
    private ServiceInfoMapper mapper;

    @Resource
    private CoreV1Api coreV1Api;

    @Override
    public List<ServiceInfo> listServiceInfo() {
        ServiceInfoExample example = new ServiceInfoExample();
        List<ServiceInfo> serviceInfoList = mapper.selectByExample(example);
        serviceInfoList.sort(Comparator.comparing(ServiceInfo::getNamespace));
        return serviceInfoList;
    }

    @Override
    public ServiceInfo getServiceInfoByServiceNameAndNamespace(String serviceName,String namespace) {
        ServiceInfoExample example = new ServiceInfoExample();
        example.createCriteria().andServiceNameEqualTo(serviceName).andNamespaceEqualTo(namespace);
        List<ServiceInfo> serviceInfoList = mapper.selectByExample(example);
        if(serviceInfoList==null || serviceInfoList.isEmpty()){
            return null;
        }else {
            return serviceInfoList.get(0);
        }
    }

    @Override
    public int updateServiceInfo(ServiceInfo serviceInfo) {
        ServiceInfoExample example = new ServiceInfoExample();
        example.createCriteria().andServiceNameEqualTo(serviceInfo.getServiceName()).andNamespaceEqualTo(serviceInfo.getNamespace());
        return mapper.updateByExampleSelective(serviceInfo,example);
    }

    @Override
    public int insertServiceInfo(ServiceInfo serviceInfo) {
        return mapper.insert(serviceInfo);
    }

    @Override
    public int deleteServiceInfo(ServiceInfo serviceInfo) {
        ServiceInfoExample example = new ServiceInfoExample();
        example.createCriteria().andServiceNameEqualTo(serviceInfo.getServiceName()).andNamespaceEqualTo(serviceInfo.getNamespace());
        return mapper.deleteByExample(example);
    }

    @Override
    public List<ServiceInfo> listServiceInfoFromCluster() {
        List<ServiceInfo> serviceInfoList = new ArrayList<>();
        try {
            V1ServiceList v1ServiceList = coreV1Api.listServiceForAllNamespaces( null, null, null, null, null, null,
                    null, null, null);
            for(V1Service v1Service:v1ServiceList.getItems()){
                serviceInfoList.add(K8sElementInfoUtil.V1ServiceToServiceInfo(v1Service));
            }
            return serviceInfoList;
        } catch (Exception e){
            log.info(e.getMessage());
        }
        return serviceInfoList;
    }
}
