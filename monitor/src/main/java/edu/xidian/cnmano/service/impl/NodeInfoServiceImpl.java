package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.api.MetricsServerApi;
import edu.xidian.cnmano.dao.NodeInfoMapper;
import edu.xidian.cnmano.entities.monitor.MetricsNode;
import edu.xidian.cnmano.entities.monitor.MetricsNodeList;
import edu.xidian.cnmano.entities.monitor.NodeInfo;
import edu.xidian.cnmano.entities.monitor.NodeInfoExample;
import edu.xidian.cnmano.service.NodeInfoService;
import edu.xidian.cnmano.util.K8sElementInfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Node相关的Service,主要负责NodeInfo相关的增删改查
 */
@Slf4j
@Service
public class NodeInfoServiceImpl implements NodeInfoService {

    @Resource
    private NodeInfoMapper mapper;

    @Resource
    private MetricsServerApi metricsServerApi;

    @Override
    public NodeInfo getNodeInfoById(int id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public NodeInfo getNodeInfoByNodeName(String nodeName) {
        NodeInfoExample example = new NodeInfoExample();
        example.createCriteria().andNodeNameEqualTo(nodeName);
        List<NodeInfo> nodeInfos = mapper.selectByExample(example);
        if(nodeInfos==null || nodeInfos.isEmpty()){
            return null;
        }else {
            return nodeInfos.get(0);
        }
    }

    @Override
    public List<NodeInfo> listAllNodeInfo() {
        NodeInfoExample example = new NodeInfoExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int updateNodeInfo(NodeInfo nodeInfo) {
        try {
            NodeInfoExample example = new NodeInfoExample();
            example.createCriteria().andNodeNameEqualTo(nodeInfo.getNodeName());
            return mapper.updateByExampleSelective(nodeInfo,example);
        }catch (Exception e){
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public int insertNodeInfo(NodeInfo nodeInfo) {
        try {
            return mapper.insert(nodeInfo);
        }catch (Exception e){
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public int deleteNodeInfo(NodeInfo nodeInfo) {
        try {
            NodeInfoExample example = new NodeInfoExample();
            example.createCriteria().andNodeNameEqualTo(nodeInfo.getNodeName());
            return mapper.deleteByExample(example);
        }catch (Exception e){
            log.error(e.getMessage());
            return 0;
        }
    }

    /**
     *
     * @return Node
     */
    @Override
    public List<NodeInfo> listNodeMetricsInfoFromCluster() {
        List<NodeInfo> nodeInfoList = new ArrayList<>();
        try {
            //NodeInformer已经获取了node的绝大部分信息，只有资源占用部分还没获取
            MetricsNodeList nodesAndUsage = metricsServerApi.listMetricsNode();
            for(MetricsNode metricsNode:nodesAndUsage.getItems()){
                nodeInfoList.add(K8sElementInfoUtil.MetricsNodeToNodeInfo(metricsNode));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nodeInfoList;
    }
}
