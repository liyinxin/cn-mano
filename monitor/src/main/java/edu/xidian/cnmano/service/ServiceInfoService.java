package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.monitor.ServiceInfo;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/17-10:59
 */
public interface ServiceInfoService {
    List<ServiceInfo> listServiceInfo();

    ServiceInfo getServiceInfoByServiceNameAndNamespace(String serviceName,String namespace);

    int updateServiceInfo(ServiceInfo serviceInfo);

    int insertServiceInfo(ServiceInfo serviceInfo);

    int deleteServiceInfo(ServiceInfo serviceInfo);

    List<ServiceInfo> listServiceInfoFromCluster();

}
