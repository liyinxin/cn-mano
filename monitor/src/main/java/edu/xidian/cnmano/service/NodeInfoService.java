package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.monitor.NodeInfo;

import java.util.List;

public interface NodeInfoService {
    public NodeInfo getNodeInfoById(int id);

    public NodeInfo getNodeInfoByNodeName(String nodeName);

    public List<NodeInfo> listAllNodeInfo();

    public int updateNodeInfo(NodeInfo nodeInfo);

    public int insertNodeInfo(NodeInfo nodeInfo);

    public int deleteNodeInfo(NodeInfo nodeInfo);

    public List<NodeInfo> listNodeMetricsInfoFromCluster();
}
