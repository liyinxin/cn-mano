package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2020/12/17-11:11
 */
@SpringBootApplication
public class OrchestratorMain8002 {
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
        SpringApplication.run(OrchestratorMain8002.class,args);
    }
}
