package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsorchestrate.Nsinstance;
import edu.xidian.cnmano.requestbody.ScaleNsRequest;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-15:01
 */
public interface NsinstanceService {
    List<Nsinstance> listNsinstance();

    int createNsinstace(Nsinstance nsinstance);

    int deleteNsinstace(Integer id);

    int terminateNsinstace(Integer id);

    int instantiateNsinstace(Integer id);

    int scaleNsinstace(ScaleNsRequest scaleNsRequest);
}
