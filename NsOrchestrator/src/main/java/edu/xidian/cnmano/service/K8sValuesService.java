package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/19-19:18
 */
public interface K8sValuesService {
    List<K8svalues> listK8sValues();


    String k8sValuesToFile(Integer id);


    K8svalues getK8sValuesById(Integer id);
}
