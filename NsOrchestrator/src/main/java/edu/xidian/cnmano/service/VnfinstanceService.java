package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsorchestrate.Vnfinstance;
import edu.xidian.cnmano.requestbody.ScaleVnfData;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-15:00
 */
public interface VnfinstanceService {
    List<Vnfinstance> listVnfinstance();

    int createVnfinstance(Vnfinstance vnfinstance);

    int terminateVnfinstance(int id, String nsInstanceName);

    int instantiateVnfinstance(int id, String nsInstanceName);

    int deleteVnfinstance(int id);

    int scaleVnfinstance(ScaleVnfData scaleVnfData, String nsInstanceName);

    int scaleVnfinstanceTest(ScaleVnfData scaleVnfData);
}
