package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsorchestrate.Nsinstance;
import edu.xidian.cnmano.requestbody.ScaleNsRequest;
import edu.xidian.cnmano.requestbody.ScaleVnfData;
import edu.xidian.cnmano.service.NsinstanceService;
import edu.xidian.cnmano.service.VnfinstanceService;
import edu.xidian.cnmano.utils.SystemCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-15:03
 */
@RestController
@Slf4j
@RequestMapping("/ns_instances")
public class NsinstanceController {
    @Resource
    private NsinstanceService nsinstanceService;

    @RequestMapping("")
    public List<Nsinstance> listNsinstance(){
        return nsinstanceService.listNsinstance();
    }

    /**
     * 前端提交的信息只有nsInstanceName，nsInstanceDescription，nsdId
     *
     * 除了创建Nsinstance之外，还需要根据nsd中的多个vnfd，分别创建vnfinstance
     * @param nsinstance
     * @return
     */
    @RequestMapping("/create")
    public int createNsinstace(Nsinstance nsinstance){
        return nsinstanceService.createNsinstace(nsinstance);
    }

    @RequestMapping("/instantiate")
    public int instantiateNsinstace(@RequestParam("id") Integer id){
        return nsinstanceService.instantiateNsinstace(id);
    }

    @RequestMapping("/scale")
    public int scaleNsinstace(ScaleNsRequest scaleNsRequest){
        return nsinstanceService.scaleNsinstace(scaleNsRequest);
    }

    @RequestMapping("/terminate")
    public int terminateNsinstace(@RequestParam("id") Integer id){
        return nsinstanceService.terminateNsinstace(id);
    }

    @RequestMapping("/delete")
    public int deleteNsinstace(@RequestParam("id") Integer id){
        return nsinstanceService.deleteNsinstace(id);
    }
}
