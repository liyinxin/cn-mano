package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsorchestrate.Vnfinstance;
import edu.xidian.cnmano.requestbody.ScaleVnfData;
import edu.xidian.cnmano.service.VnfinstanceService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-15:02
 */
@RestController
@RequestMapping("/vnf_instances")
public class VnfinstanceController {

    @Resource
    VnfinstanceService vnfinstanceService;

    @RequestMapping("")
    public List<Vnfinstance> listVnfinstance(){
        return vnfinstanceService.listVnfinstance();
    }

    @RequestMapping("/create")
    public int createVnfinstance(Vnfinstance vnfinstance){
        return vnfinstanceService.createVnfinstance(vnfinstance);
    }

    @RequestMapping("/instantiate")
    public int instantiateVnfinstance(@RequestParam("id") int id){
        return vnfinstanceService.instantiateVnfinstance(id,"default");
    }

    @RequestMapping("/scale")
    public int scaleVnfinstance(ScaleVnfData scaleVnfData){
        return vnfinstanceService.scaleVnfinstance(scaleVnfData,"default");
    }

    @RequestMapping("/terminate")
    public int terminateVnfinstance(@RequestParam("id") int id){
        return vnfinstanceService.terminateVnfinstance(id,"default");
    }

    @RequestMapping("/delete")
    public int deleteVnfinstance(@RequestParam("id") int id){
        return vnfinstanceService.deleteVnfinstance(id);
    }

    @RequestMapping("/scaleTest")
    public int scaleVnfinstanceTest(ScaleVnfData scaleVnfData){
        return vnfinstanceService.scaleVnfinstanceTest(scaleVnfData);
    }
}
