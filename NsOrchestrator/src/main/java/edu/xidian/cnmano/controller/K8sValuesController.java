package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;
import edu.xidian.cnmano.service.K8sValuesService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/17-17:20
 */
@RestController
@RequestMapping("/k8svalues")
public class K8sValuesController {

    @Resource
    private K8sValuesService k8sValuesService;

    @RequestMapping("")
    public List<K8svalues> listK8sValues() {
        return k8sValuesService.listK8sValues();
    }

    @RequestMapping("/toFile")
    public String k8sValuesToFile(@RequestParam("id") Integer id) {
        return k8sValuesService.k8sValuesToFile(id);
    }

}
