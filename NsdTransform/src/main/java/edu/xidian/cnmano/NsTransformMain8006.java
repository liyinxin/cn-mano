package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2021/1/16-15:30
 */
@SpringBootApplication
public class NsTransformMain8006 {
    public static void main(String[] args) {
        SpringApplication.run(NsTransformMain8006.class);
    }
}
