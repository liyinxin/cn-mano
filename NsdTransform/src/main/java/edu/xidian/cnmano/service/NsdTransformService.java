package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;
import edu.xidian.cnmano.entities.nstransform.VnfInstanceFull;

/**
 * @author zhr
 * @date 2021/1/16-19:46
 */
public interface NsdTransformService {
    K8svalues transVnfInstanceTok8sValue(VnfInstanceFull vnfInstanceFull);

    K8svalues transOsContainerDescTok8sValue(Oscontainerdesc oscontainerdesc);
}
