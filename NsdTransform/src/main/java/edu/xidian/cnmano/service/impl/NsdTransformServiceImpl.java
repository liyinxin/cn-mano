package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.entities.nsdmanagement.*;
import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;
import edu.xidian.cnmano.entities.nstransform.*;
import edu.xidian.cnmano.service.NsdTransformService;
import org.springframework.stereotype.Service;

/**
 * @author zhr
 * @date 2021/1/16-19:46
 */
@Service
public class NsdTransformServiceImpl implements NsdTransformService {

    @Override
    public K8svalues transVnfInstanceTok8sValue(VnfInstanceFull vnfInstanceFull) {
        K8svalues k8svalues = new K8svalues();
        k8svalues.setVnfInstanceId(vnfInstanceFull.getId());

        k8svalues.setDeploymentDescription(vnfInstanceFull.getVnfInstanceDescription());

        VirtualizednetworkfunctiondescriptorFull vnfd = vnfInstanceFull.getVnfd();
        k8svalues.setName(vnfd.getVnfProductName());

        VirtualdeploymentunitFull vdu = vnfd.getVdu().get(0);
        VduprofileFull vduProfile = vnfInstanceFull.getFlavour().getVduProfile().get(0);
        Virtualcpd virtualCpd = vnfd.getVirtualCpd().get(0);

        Vducpd vducpd = vdu.getIntCpd().get(0);
        OscontainerdescFull oscontainerdesc = vdu.getOsContainerDesc().get(0);
        Swimagedesc swImageDesc = oscontainerdesc.getSwImageDesc();

        Affinityorantiaffinitygroup affinityGroup = vduProfile.getAffinityOrAntiAffinityGroup().get(0);
        Localaffinityorantiaffinityrule localAffinity = vduProfile.getLocalAffinityOrAntiAffinityRule().get(0);

        k8svalues.setPodDescription(vdu.getDescription());
        k8svalues.setBitrateRequirement(vducpd.getBitrateRequirement()+"M");
        k8svalues.setContainerPort(vducpd.getPort());
        k8svalues.setImageUrl(swImageDesc.getSwImage());

        k8svalues.setRequestCpu(oscontainerdesc.getRequestedCpuResources()+"m");
        k8svalues.setRequestMemory(oscontainerdesc.getRequestedMemoryResources()+"Mi");
        k8svalues.setCpuLimit(oscontainerdesc.getCpuResourceLimit()+"m");
        k8svalues.setMemoryLimit(oscontainerdesc.getMemoryResourceLimit()+"Mi");

        k8svalues.setPortName(virtualCpd.getName());
        k8svalues.setServicePort(virtualCpd.getPort());
        k8svalues.setL4protocol(virtualCpd.getProtocol());

        k8svalues.setReplicas(vduProfile.getMinNumberOfInstances());
        k8svalues.setPodAffinity(affinityGroup.getAffinityOrAntiAffinity());
        k8svalues.setLocation(localAffinity.getScope());

        return k8svalues;
    }

    @Override
    public K8svalues transOsContainerDescTok8sValue(Oscontainerdesc oscontainerdesc) {
        K8svalues k8svalues = new K8svalues();
        k8svalues.setRequestCpu(oscontainerdesc.getRequestedCpuResources()+"m");
        k8svalues.setRequestMemory(oscontainerdesc.getRequestedMemoryResources()+"Mi");
        k8svalues.setCpuLimit(oscontainerdesc.getCpuResourceLimit()+"m");
        k8svalues.setMemoryLimit(oscontainerdesc.getMemoryResourceLimit()+"Mi");
        return k8svalues;
    }
}
