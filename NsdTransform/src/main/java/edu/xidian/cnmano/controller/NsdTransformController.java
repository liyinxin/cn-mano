package edu.xidian.cnmano.controller;

import com.google.gson.Gson;
import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;
import edu.xidian.cnmano.entities.nstransform.VnfInstanceFull;
import edu.xidian.cnmano.service.NsdTransformService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhr
 * @date 2021/1/16-19:45
 */
@RestController
public class NsdTransformController {

    @Resource
    NsdTransformService nsdTransformService;

    @PostMapping("/trans_vnfInstance_tok8sValue")
    public K8svalues transVnfInstanceTok8sValue(@RequestBody VnfInstanceFull vnfInstanceFull){
        Gson gson = new Gson();
        String str = gson.toJson(vnfInstanceFull);
        System.out.println(str);
        return nsdTransformService.transVnfInstanceTok8sValue(vnfInstanceFull);
    }

    @PostMapping("/trans_oscontainerdesc_tok8sValue")
    public K8svalues transOsContainerDescTok8sValue(@RequestBody Oscontainerdesc oscontainerdesc){
        return nsdTransformService.transOsContainerDescTok8sValue(oscontainerdesc);
    }
}
