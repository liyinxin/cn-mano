package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.templatemanagement.Template;
import edu.xidian.cnmano.service.TemplateService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2020/12/17-19:48
 */
@RestController
@RequestMapping("/template")
public class TemplateController {

    @Resource
    private TemplateService templateService;

    @RequestMapping("")
    public List<Template> listTemplate(){
        return templateService.listTemplate();
    }

    @RequestMapping("/delete")
    public int deleteTemplate(@RequestParam("id") Integer id){
        return templateService.deleteTemplateById(id);
    }

    @RequestMapping("/update")
    public int updateTemplate(Template template){
        return templateService.updateTemplate(template);
    }

    @RequestMapping("/create")
    public int createTemplate(Template template){
        return templateService.createTemplate(template);
    }
}
