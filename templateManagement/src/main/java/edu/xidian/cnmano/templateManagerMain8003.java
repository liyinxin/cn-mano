package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2020/12/17-16:30
 */
@SpringBootApplication
public class templateManagerMain8003 {
    public static void main(String[] args) {
        SpringApplication.run(templateManagerMain8003.class,args);
    }
}
