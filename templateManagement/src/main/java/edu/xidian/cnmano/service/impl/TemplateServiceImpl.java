package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.TemplateMapper;
import edu.xidian.cnmano.entities.templatemanagement.Template;
import edu.xidian.cnmano.entities.templatemanagement.TemplateExample;
import edu.xidian.cnmano.service.TemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2020/12/17-19:48
 */
@Slf4j
@Service
public class TemplateServiceImpl implements TemplateService {

    @Resource
    private TemplateMapper mapper;

    @Override
    public int createTemplate(Template template) {
        return mapper.insert(template);
    }

    @Override
    public int updateTemplate(Template template) {
        if(template.getId()==null || template.getId()<=0){
            return createTemplate(template);
        }else {
            return mapper.updateByPrimaryKeySelective(template);
        }
    }

    @Override
    public List<Template> listTemplate() {
        TemplateExample example = new TemplateExample();
        return mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public int deleteTemplateById(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }
}
