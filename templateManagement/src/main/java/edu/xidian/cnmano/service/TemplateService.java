package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.templatemanagement.Template;

import java.util.List;

/**
 * @author zhr
 * @date 2020/12/17-19:47
 */
public interface TemplateService {
    int createTemplate(Template template);

    int updateTemplate(Template template);

    List<Template> listTemplate();

    int deleteTemplateById(Integer id);
}
