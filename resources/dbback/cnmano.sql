-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: cnmano
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AffinityOrAntiAffinityGroup`
--

DROP TABLE IF EXISTS `AffinityOrAntiAffinityGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AffinityOrAntiAffinityGroup` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  `affinityOrAntiAffinity` varchar(255) DEFAULT NULL COMMENT '表明成员之间的关系，AFFINITY或ANTI_AFFINITY之一',
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AffinityOrAntiAffinityGroup`
--

LOCK TABLES `AffinityOrAntiAffinityGroup` WRITE;
/*!40000 ALTER TABLE `AffinityOrAntiAffinityGroup` DISABLE KEYS */;
INSERT INTO `AffinityOrAntiAffinityGroup` VALUES (1,'In'),(2,'NotIn');
/*!40000 ALTER TABLE `AffinityOrAntiAffinityGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `K8sValues`
--

DROP TABLE IF EXISTS `K8sValues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `K8sValues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vnfInstanceId` int(11) DEFAULT NULL COMMENT '此数据对应的vnfInstance的id',
  `name` varchar(255) DEFAULT NULL COMMENT 'pod，service，deployment的名称，各种标签的统一命名。',
  `deploymentDescription` varchar(255) DEFAULT NULL COMMENT 'Deployment的描述',
  `replicas` int(11) DEFAULT NULL COMMENT 'pod的副本数',
  `bitrateRequirement` varchar(255) DEFAULT NULL COMMENT 'pod的网络带宽限制，一般单位为M',
  `podDescription` varchar(255) DEFAULT NULL COMMENT 'pod的描述',
  `imageUrl` varchar(255) DEFAULT NULL COMMENT '镜像的Url',
  `portName` varchar(255) DEFAULT NULL COMMENT '端口的协议名，http之类的',
  `containerPort` int(11) DEFAULT NULL COMMENT '容器对外服务的端口',
  `servicePort` int(11) DEFAULT NULL COMMENT 'service的ClusterIp端口',
  `l4protocol` varchar(255) DEFAULT NULL COMMENT '容器/service第三层的协议，一般为TCP，或为UDP',
  `requestCpu` varchar(255) DEFAULT NULL COMMENT '请求的cpu量，一般单位为m（千分之一个cpu）',
  `requestMemory` varchar(255) DEFAULT NULL COMMENT '请求的memory量，一般单位为Mi',
  `cpuLimit` varchar(255) DEFAULT NULL COMMENT 'cpu使用的限制量，一般单位为m（千分之一个cpu）',
  `memoryLimit` varchar(255) DEFAULT NULL COMMENT '限制的memory使用量，一般单位为Mi',
  `location` varchar(255) DEFAULT NULL COMMENT 'pod的地理位置偏向性，EDGE或CLOUD',
  `podAffinity` varchar(255) DEFAULT NULL COMMENT '同类型pod之间的亲和性，In或NotIn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `K8sValues`
--

LOCK TABLES `K8sValues` WRITE;
/*!40000 ALTER TABLE `K8sValues` DISABLE KEYS */;
INSERT INTO `K8sValues` VALUES (46,297,'nrf','vnfInstance-inside-demo',2,'10M','nrf_pod','192.168.10.60/ien-free5gc/nrf:v2','sbi',29510,29510,'TCP','100m','100Mi','100m','100Mi','CLOUD','NotIn'),(47,298,'amf','vnfInstance-inside-demo',2,'10M','amf_pod','192.168.10.60/ien-free5gc/amf:v2','sbi',29518,29518,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(48,299,'nssf','vnfInstance-inside-demo',1,'10M','nssf_pod','192.168.10.60/ien-free5gc/nssf:v2','sbi',29531,29531,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(49,300,'pcf','vnfInstance-inside-demo',1,'10M','pcf_pod','192.168.10.60/ien-free5gc/pcf:v2','sbi',29507,29507,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(50,301,'udm','vnfInstance-inside-demo',1,'10M','udm_pod','192.168.10.60/ien-free5gc/udm:v2','sbi',29503,29503,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(51,302,'smf','vnfInstance-inside-demo',2,'10M','smf_pod','192.168.10.60/ien-free5gc/smf:v2','sbi',29502,29502,'TCP','100m','100Mi','100m','100Mi','EDGE','NotIn'),(52,303,'udr','vnfInstance-inside-demo',1,'10M','udr_pod','192.168.10.60/ien-free5gc/udr:v2','sbi',29504,29504,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(53,304,'ausf','vnfInstance-inside-demo',1,'10M','ausf_pod','192.168.10.60/ien-free5gc/ausf:v2','sbi',29509,29509,'TCP','100m','100Mi','100m','100Mi','CLOUD','In'),(54,305,'upf','vnfInstance-inside-demo',3,'10M','upf_pod','192.168.10.60/ien-free5gc/upf:v2','pfcp',8805,8805,'UDP','100m','100Mi','100m','100Mi','EDGE','NotIn');
/*!40000 ALTER TABLE `K8sValues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LocalAffinityOrAntiAffinityRule`
--

DROP TABLE IF EXISTS `LocalAffinityOrAntiAffinityRule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LocalAffinityOrAntiAffinityRule` (
  `ruleId` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个亲和性rule',
  `type` varchar(255) DEFAULT NULL COMMENT 'Affinity，k8s目前仅支持节点亲和',
  `scope` varchar(255) DEFAULT NULL COMMENT 'EDGE/CLOUD',
  PRIMARY KEY (`ruleId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LocalAffinityOrAntiAffinityRule`
--

LOCK TABLES `LocalAffinityOrAntiAffinityRule` WRITE;
/*!40000 ALTER TABLE `LocalAffinityOrAntiAffinityRule` DISABLE KEYS */;
INSERT INTO `LocalAffinityOrAntiAffinityRule` VALUES (1,'AFFINITY','EDGE'),(2,'AFFINITY','CLOUD');
/*!40000 ALTER TABLE `LocalAffinityOrAntiAffinityRule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NetworkServiceDescriptor`
--

DROP TABLE IF EXISTS `NetworkServiceDescriptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NetworkServiceDescriptor` (
  `nsdId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'NSD的标识符，全局唯一标识nsd',
  `designer` varchar(255) DEFAULT NULL COMMENT '此NSD的设计者',
  `version` varchar(255) DEFAULT NULL COMMENT 'NSD的版本',
  `nsdName` varchar(255) DEFAULT NULL COMMENT 'NSD的名称',
  `vnfdId` varchar(255) DEFAULT NULL COMMENT '此NS包含的VNFD的id，以逗号分隔',
  PRIMARY KEY (`nsdId`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NetworkServiceDescriptor`
--

LOCK TABLES `NetworkServiceDescriptor` WRITE;
/*!40000 ALTER TABLE `NetworkServiceDescriptor` DISABLE KEYS */;
INSERT INTO `NetworkServiceDescriptor` VALUES (16,'zhr','1.0','free5gc-core','8,9,10,11,12,14,15,16,17'),(114,'zhr','1.0','cn1nf','56'),(115,'zhr','1.0','cn2nf','57'),(116,'zhr','1.0','cn3nf','58'),(117,'zhr','1.0','cn4nf','59'),(118,'zhr','1.0','cn5nf','60'),(119,'zhr','1.0','cn15nf','61'),(120,'zhr','1.0','cn30nf','62'),(121,'zhr','1.0','embb-core-network','8,9,10,11,12,14,15,16,17'),(172,'zhr','1.0','urllc','8,9,14,17'),(173,'zhr','1.0','core-network','8,9,10,11,12,14,15,16,17');
/*!40000 ALTER TABLE `NetworkServiceDescriptor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NsInstance`
--

DROP TABLE IF EXISTS `NsInstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NsInstance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个NS实例',
  `nsInstanceName` varchar(255) DEFAULT NULL COMMENT 'NSInstance的名称',
  `nsInstanceDescription` varchar(255) DEFAULT NULL COMMENT '此NSInstace的描述',
  `nsdId` int(11) DEFAULT NULL COMMENT '相关的nsd的id',
  `vnfInstance` varchar(255) DEFAULT NULL COMMENT 'VnfInstance引用，id逗号隔开',
  `nsState` varchar(255) DEFAULT NULL COMMENT 'NS的状态，NOT_INSTANTIATED或INSTANTIATED',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NsInstance`
--

LOCK TABLES `NsInstance` WRITE;
/*!40000 ALTER TABLE `NsInstance` DISABLE KEYS */;
INSERT INTO `NsInstance` VALUES (58,'core-network','core-network',173,'252,253,254,255,256,257,258,259,260','NOT_INSTANTIATED'),(62,'free5gc-core','free5gc-core',16,'288,289,290,291,292,293,294,295,296','NOT_INSTANTIATED'),(63,'demo','demo',16,'297,298,299,300,301,302,303,304,305','INSTANTIATED');
/*!40000 ALTER TABLE `NsInstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OsContainerDesc`
--

DROP TABLE IF EXISTS `OsContainerDesc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OsContainerDesc` (
  `osContainerDescId` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个osContainerDesc',
  `requestedCpuResources` int(11) DEFAULT NULL COMMENT '此容器需求的CPU资源',
  `requestedMemoryResources` int(11) DEFAULT NULL COMMENT '此容器需求的内存资源',
  `cpuResourceLimit` int(11) DEFAULT NULL COMMENT '此容器最多可以使用的CPU资源',
  `memoryResourceLimit` int(11) DEFAULT NULL COMMENT '此容器最多可以使用的内存资源',
  `swImageDesc` int(11) DEFAULT NULL COMMENT '描述实现此容器的镜像，swImageDescId',
  PRIMARY KEY (`osContainerDescId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OsContainerDesc`
--

LOCK TABLES `OsContainerDesc` WRITE;
/*!40000 ALTER TABLE `OsContainerDesc` DISABLE KEYS */;
INSERT INTO `OsContainerDesc` VALUES (5,100,100,100,100,4),(6,100,100,100,100,5),(7,100,100,100,100,6),(8,100,100,100,100,7),(9,100,100,100,100,8),(10,100,100,100,100,9),(12,100,100,100,100,10),(13,100,100,100,100,11),(14,100,100,100,100,12);
/*!40000 ALTER TABLE `OsContainerDesc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SwImageDesc`
--

DROP TABLE IF EXISTS `SwImageDesc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SwImageDesc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个镜像',
  `name` varchar(255) DEFAULT NULL COMMENT '此镜像的名称',
  `version` varchar(255) DEFAULT NULL COMMENT '此镜像的版本',
  `swImage` varchar(255) DEFAULT NULL COMMENT '对实际软件镜像的引用，此引用可以为到VNF包根目录的相对路径或者一个URL',
  `size` varchar(255) DEFAULT NULL COMMENT '此镜像的大小',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SwImageDesc`
--

LOCK TABLES `SwImageDesc` WRITE;
/*!40000 ALTER TABLE `SwImageDesc` DISABLE KEYS */;
INSERT INTO `SwImageDesc` VALUES (4,'nrf','v2','192.168.10.60/ien-free5gc/nrf:v2','12.48M'),(5,'amf','v2','192.168.10.60/ien-free5gc/amf:v2','12.60M'),(6,'nssf','v2','192.168.10.60/ien-free5gc/nssf:v2','10.57M'),(7,'pcf','v2','192.168.10.60/ien-free5gc/pcf:v2','11.26M'),(8,'udm','v2','192.168.10.60/ien-free5gc/udm:v2','11.15M'),(9,'smf','v2','192.168.10.60/ien-free5gc/smf:v2','11.94M'),(10,'udr','v2','192.168.10.60/ien-free5gc/udr:v2','12.86M'),(11,'ausf','v2','192.168.10.60/ien-free5gc/ausf:v2','11.40M'),(12,'upf','v2','192.168.10.60/ien-free5gc/upf:v2','51.78M');
/*!40000 ALTER TABLE `SwImageDesc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VduCpd`
--

DROP TABLE IF EXISTS `VduCpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VduCpd` (
  `cpdId` int(11) NOT NULL AUTO_INCREMENT,
  `layerProtocol` varchar(255) DEFAULT NULL COMMENT '接入点使用的协议，Ethernet、MPLS、ODU2、IPV4、IPV6、Pseudo-Wire等等.',
  `bitrateRequirement` int(11) DEFAULT NULL,
  `port` int(11) DEFAULT NULL COMMENT '容器对外暴露服务的端口',
  PRIMARY KEY (`cpdId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VduCpd`
--

LOCK TABLES `VduCpd` WRITE;
/*!40000 ALTER TABLE `VduCpd` DISABLE KEYS */;
INSERT INTO `VduCpd` VALUES (5,'ipv4',10,29510),(6,'ipv4',10,29509),(7,'ipv4',10,29518),(8,'ipv4',10,29507),(9,'ipv4',10,29502),(10,'ipv4',10,29503),(11,'ipv4',10,29504),(12,'ipv4',10,29531),(13,'ipv4',10,8805);
/*!40000 ALTER TABLE `VduCpd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VduProfile`
--

DROP TABLE IF EXISTS `VduProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VduProfile` (
  `vduProfileId` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个vduProfile',
  `vduId` int(11) DEFAULT NULL COMMENT '唯一标识一个VDU',
  `minNumberOfInstances` int(11) DEFAULT NULL COMMENT '基于此VDU，VNFC实例的最少数目',
  `maxNumberOfInstances` int(11) DEFAULT NULL COMMENT '基于此VDU，VNFC实例的最多数目',
  `affinityOrAntiAffinityGroupId` varchar(255) DEFAULT NULL COMMENT '亲和组或反亲和组（pod间的），id通过逗号分隔',
  `localAffinityOrAntiAffinityRuleId` varchar(255) DEFAULT NULL COMMENT '节点亲和或反亲和，id逗号分隔',
  PRIMARY KEY (`vduProfileId`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VduProfile`
--

LOCK TABLES `VduProfile` WRITE;
/*!40000 ALTER TABLE `VduProfile` DISABLE KEYS */;
INSERT INTO `VduProfile` VALUES (3,4,2,2,'2','2'),(4,5,2,2,'1','2'),(5,6,1,1,'1','2'),(6,7,1,1,'1','2'),(7,8,1,1,'1','2'),(8,9,2,2,'2','1'),(9,10,1,1,'1','2'),(10,11,1,1,'1','2'),(11,12,3,3,'2','1'),(12,4,1,1,'1','2'),(13,4,2,2,'1','2'),(14,4,3,3,'1','2'),(15,4,4,4,'1','2'),(16,4,5,5,'1','2'),(17,4,6,6,'1','2'),(18,4,7,7,'1','2'),(19,4,8,8,'1','2'),(20,4,9,9,'1','2'),(21,4,10,10,'1','2'),(22,4,11,11,'1','2'),(23,4,12,12,'1','2'),(24,4,13,13,'1','2'),(27,4,14,14,'1','2'),(28,4,15,15,'1','2'),(29,4,16,16,'1','2'),(30,4,17,17,'1','2'),(31,4,18,18,'1','2'),(32,4,19,19,'1','2'),(33,4,20,20,'1','2'),(34,4,21,21,'1','2'),(35,4,22,22,'1','2'),(36,4,23,23,'1','2'),(37,4,24,24,'1','2'),(38,4,25,25,'1','2'),(39,4,26,26,'1','2'),(40,4,27,27,'1','2'),(41,4,28,28,'1','2'),(42,4,29,29,'1','2'),(43,4,30,30,'1','2');
/*!40000 ALTER TABLE `VduProfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualCpd`
--

DROP TABLE IF EXISTS `VirtualCpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualCpd` (
  `cpdId` int(11) NOT NULL AUTO_INCREMENT,
  `layerProtocol` varchar(255) DEFAULT NULL COMMENT '接入点使用的协议，Ethernet、MPLS、ODU2、IPV4、IPV6、Pseudo-Wire等等.',
  `vdu` varchar(255) DEFAULT NULL COMMENT '引用实现了该服务的VDU,id通过逗号分隔',
  `name` varchar(255) DEFAULT NULL COMMENT 'VirtualCp暴露的端口的名称',
  `protocol` varchar(255) DEFAULT NULL COMMENT '第四层协议，TCP',
  `port` int(11) DEFAULT NULL COMMENT '暴露的端口号',
  PRIMARY KEY (`cpdId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualCpd`
--

LOCK TABLES `VirtualCpd` WRITE;
/*!40000 ALTER TABLE `VirtualCpd` DISABLE KEYS */;
INSERT INTO `VirtualCpd` VALUES (3,'ipv4','4','sbi','TCP',29510),(4,'ipv4','5','sbi','TCP',29518),(5,'ipv4','6','sbi','TCP',29531),(6,'ipv4','7','sbi','TCP',29507),(7,'ipv4','8','sbi','TCP',29503),(8,'ipv4','9','sbi','TCP',29502),(9,'ipv4','9','pfcp','UDP',8805),(10,'ipv4','10','sbi','TCP',29504),(11,'ipv4','11','sbi','TCP',29509),(12,'ipv4','12','pfcp','UDP',8805);
/*!40000 ALTER TABLE `VirtualCpd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualDeploymentUnit`
--

DROP TABLE IF EXISTS `VirtualDeploymentUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualDeploymentUnit` (
  `vduId` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个VDU',
  `name` varchar(255) DEFAULT NULL COMMENT 'VDU名称',
  `description` varchar(255) DEFAULT NULL COMMENT 'VDU的描述',
  `intCpd` varchar(255) DEFAULT NULL COMMENT '描述VNFC实例之间的虚拟网络链接，VduCpdid，逗号分隔',
  `osContainerDesc` varchar(255) DEFAULT NULL COMMENT '规定容器的CPU、内存的需求/限制、软件镜像，这些容器共享相同的主机，在相同的网络名称空间下，id逗号分隔',
  PRIMARY KEY (`vduId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualDeploymentUnit`
--

LOCK TABLES `VirtualDeploymentUnit` WRITE;
/*!40000 ALTER TABLE `VirtualDeploymentUnit` DISABLE KEYS */;
INSERT INTO `VirtualDeploymentUnit` VALUES (4,'nrf','nrf_pod','5','5'),(5,'amf','amf_pod','7','6'),(6,'nssf','nssf_pod','12','7'),(7,'pcf','pcf_pod','8','8'),(8,'udm','udm_pod','10','9'),(9,'smf','smf_pod','9,13','10'),(10,'udr','udr_pod','11','12'),(11,'ausf','ausf_pod','6','13'),(12,'upf','upf_pod','13','14');
/*!40000 ALTER TABLE `VirtualDeploymentUnit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualizedNetworkFunctionDeploymentFlavour`
--

DROP TABLE IF EXISTS `VirtualizedNetworkFunctionDeploymentFlavour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualizedNetworkFunctionDeploymentFlavour` (
  `flavourId` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个VNFD中的DF',
  `description` varchar(255) DEFAULT NULL COMMENT '此DF的描述',
  `vduProfile` varchar(255) DEFAULT NULL COMMENT '描述此风格中，VDU实例化所需要的额外数据，id逗号分隔',
  PRIMARY KEY (`flavourId`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualizedNetworkFunctionDeploymentFlavour`
--

LOCK TABLES `VirtualizedNetworkFunctionDeploymentFlavour` WRITE;
/*!40000 ALTER TABLE `VirtualizedNetworkFunctionDeploymentFlavour` DISABLE KEYS */;
INSERT INTO `VirtualizedNetworkFunctionDeploymentFlavour` VALUES (4,'nrf部署风格','3'),(5,'amf部署风格','4'),(6,'nssf部署风格','5'),(7,'pcf部署风格','6'),(8,'udm部署风格','7'),(9,'smf部署风格','8'),(10,'udr部署风格','9'),(11,'ausf部署风格','10'),(12,'upf部署风格','11'),(13,'1个nrf','12'),(14,'2个nrf','13'),(15,'3个nrf','14'),(16,'4个nrf','15'),(17,'5个nrf','16'),(18,'6个nrf','17'),(19,'7个nrf','18'),(20,'8个nrf','19'),(21,'9个nrf','20'),(22,'10个nrf','21'),(23,'11个nrf','22'),(24,'12个nrf','23'),(25,'13个nrf','24'),(26,'14个nrf','27'),(27,'15个nrf','28'),(28,'16个nrf','29'),(29,'17个nrf','30'),(30,'18个nrf','31'),(31,'19个nrf','32'),(32,'20个nrf','33'),(33,'21个nrf','34'),(34,'22个nrf','35'),(35,'23个nrf','36'),(36,'24个nrf','37'),(37,'25个nrf','38'),(38,'26个nrf','39'),(39,'27个nrf','40'),(40,'28个nrf','41'),(41,'29个nrf','42'),(42,'30个nrf','43');
/*!40000 ALTER TABLE `VirtualizedNetworkFunctionDeploymentFlavour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualizedNetworkFunctionDescriptor`
--

DROP TABLE IF EXISTS `VirtualizedNetworkFunctionDescriptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualizedNetworkFunctionDescriptor` (
  `vnfdId` int(11) NOT NULL AUTO_INCREMENT COMMENT '此VNFD的唯一标识符',
  `vnfProvider` varchar(255) DEFAULT NULL COMMENT '此VNF以及VNF的提供者',
  `vnfProductName` varchar(255) DEFAULT NULL COMMENT '此VNF产品的名称，在VNF产品的生命周期中名称不变',
  `vnfSoftwareVersion` varchar(255) DEFAULT NULL COMMENT 'VNFD中app的版本',
  `vnfdVersion` varchar(255) DEFAULT NULL COMMENT 'VNFD的版本',
  `vdu` varchar(255) DEFAULT NULL COMMENT 'vduId，逗号分隔',
  `virtualCpd` varchar(255) DEFAULT NULL COMMENT '描述允许访问一组VNFC实例的虚拟连接点id，逗号分隔',
  `deploymentFlavour` int(11) DEFAULT NULL COMMENT '描述具有特定性能要求的VNF的特定DF（Deployment Flavour）id',
  PRIMARY KEY (`vnfdId`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualizedNetworkFunctionDescriptor`
--

LOCK TABLES `VirtualizedNetworkFunctionDescriptor` WRITE;
/*!40000 ALTER TABLE `VirtualizedNetworkFunctionDescriptor` DISABLE KEYS */;
INSERT INTO `VirtualizedNetworkFunctionDescriptor` VALUES (8,'free5gc','nrf','1.0.0','1.0','4','3',4),(9,'free5gc','amf','1.0.0','1.0','5','4',5),(10,'free5gc','nssf','1.0.0','1.0','6','5',6),(11,'free5gc','pcf','1.0.0','1.0','7','6',7),(12,'free5gc','udm','1.0.0','1.0','8','7',8),(14,'free5gc','smf','1.0.0','1.0','9','8,9',9),(15,'free5gc','udr','1.0.0','1.0','10','10',10),(16,'free5gc','ausf','1.0.0','1.0','11','11',11),(17,'free5gc','upf','1.0.0','1.0','12','12',12),(56,'free5gc','test1nf','1.0.0','1.0','4','3',13),(57,'free5gc','test2nf','1.0.0','1.0','4','3',14),(58,'free5gc','test3nf','1.0.0','1.0','4','3',15),(59,'free5gc','test4nf','1.0.0','1.0','4','3',16),(60,'free5gc','test5nf','1.0.0','1.0','4','3',17),(61,'free5gc','test15nf','1.0.0','1.0','4','3',27),(62,'free5gc','test30nf','1.0.0','1.0','4','3',42);
/*!40000 ALTER TABLE `VirtualizedNetworkFunctionDescriptor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VnfInstance`
--

DROP TABLE IF EXISTS `VnfInstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VnfInstance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vnfInstanceName` varchar(255) DEFAULT NULL COMMENT 'VNF实例的名称',
  `vnfInstanceDescription` varchar(255) DEFAULT NULL COMMENT 'VNF实例的描述',
  `vnfdId` int(11) DEFAULT NULL COMMENT '表明此VNF基于的VNFD',
  `vnfProvider` varchar(255) DEFAULT NULL COMMENT 'VNF提供者，拷贝自VNFD',
  `vnfProductName` varchar(255) DEFAULT NULL COMMENT 'VNF产品名称，拷贝自VNFD',
  `vnfSoftwareVersion` varchar(255) DEFAULT NULL COMMENT 'VNF产品名称，拷贝自VNFD',
  `vnfdVersion` varchar(255) DEFAULT NULL COMMENT 'VNFD版本',
  `instantiationState` varchar(255) DEFAULT NULL COMMENT 'VNF的实例化状况，NOT_INSTANTIATED或INSTANTIATED',
  `flavourId` int(11) DEFAULT NULL COMMENT '此VNF的部署风格id',
  `vnfState` varchar(255) DEFAULT NULL COMMENT '此VNF实例的运行状态，Pod的状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VnfInstance`
--

LOCK TABLES `VnfInstance` WRITE;
/*!40000 ALTER TABLE `VnfInstance` DISABLE KEYS */;
INSERT INTO `VnfInstance` VALUES (252,'nrf-1.0-instance','vnfInstance-inside-core-network',8,'free5gc','nrf','1.0.0','1.0','NOT_INSTANTIATED',4,'CREATED'),(253,'amf-1.0-instance','vnfInstance-inside-core-network',9,'free5gc','amf','1.0.0','1.0','NOT_INSTANTIATED',5,'CREATED'),(254,'nssf-1.0-instance','vnfInstance-inside-core-network',10,'free5gc','nssf','1.0.0','1.0','NOT_INSTANTIATED',6,'CREATED'),(255,'pcf-1.0-instance','vnfInstance-inside-core-network',11,'free5gc','pcf','1.0.0','1.0','NOT_INSTANTIATED',7,'CREATED'),(256,'udm-1.0-instance','vnfInstance-inside-core-network',12,'free5gc','udm','1.0.0','1.0','NOT_INSTANTIATED',8,'CREATED'),(257,'smf-1.0-instance','vnfInstance-inside-core-network',14,'free5gc','smf','1.0.0','1.0','NOT_INSTANTIATED',9,'CREATED'),(258,'udr-1.0-instance','vnfInstance-inside-core-network',15,'free5gc','udr','1.0.0','1.0','NOT_INSTANTIATED',10,'CREATED'),(259,'ausf-1.0-instance','vnfInstance-inside-core-network',16,'free5gc','ausf','1.0.0','1.0','NOT_INSTANTIATED',11,'CREATED'),(260,'upf-1.0-instance','vnfInstance-inside-core-network',17,'free5gc','upf','1.0.0','1.0','NOT_INSTANTIATED',12,'CREATED'),(288,'nrf-1.0-instance','vnfInstance-inside-free5gc-core',8,'free5gc','nrf','1.0.0','1.0','NOT_INSTANTIATED',4,'CREATED'),(289,'amf-1.0-instance','vnfInstance-inside-free5gc-core',9,'free5gc','amf','1.0.0','1.0','NOT_INSTANTIATED',5,'CREATED'),(290,'nssf-1.0-instance','vnfInstance-inside-free5gc-core',10,'free5gc','nssf','1.0.0','1.0','NOT_INSTANTIATED',6,'CREATED'),(291,'pcf-1.0-instance','vnfInstance-inside-free5gc-core',11,'free5gc','pcf','1.0.0','1.0','NOT_INSTANTIATED',7,'CREATED'),(292,'udm-1.0-instance','vnfInstance-inside-free5gc-core',12,'free5gc','udm','1.0.0','1.0','NOT_INSTANTIATED',8,'CREATED'),(293,'smf-1.0-instance','vnfInstance-inside-free5gc-core',14,'free5gc','smf','1.0.0','1.0','NOT_INSTANTIATED',9,'CREATED'),(294,'udr-1.0-instance','vnfInstance-inside-free5gc-core',15,'free5gc','udr','1.0.0','1.0','NOT_INSTANTIATED',10,'CREATED'),(295,'ausf-1.0-instance','vnfInstance-inside-free5gc-core',16,'free5gc','ausf','1.0.0','1.0','NOT_INSTANTIATED',11,'CREATED'),(296,'upf-1.0-instance','vnfInstance-inside-free5gc-core',17,'free5gc','upf','1.0.0','1.0','NOT_INSTANTIATED',12,'CREATED'),(297,'nrf-1.0-instance','vnfInstance-inside-demo',8,'free5gc','nrf','1.0.0','1.0','INSTANTIATED',4,'CREATED'),(298,'amf-1.0-instance','vnfInstance-inside-demo',9,'free5gc','amf','1.0.0','1.0','INSTANTIATED',5,'CREATED'),(299,'nssf-1.0-instance','vnfInstance-inside-demo',10,'free5gc','nssf','1.0.0','1.0','INSTANTIATED',6,'CREATED'),(300,'pcf-1.0-instance','vnfInstance-inside-demo',11,'free5gc','pcf','1.0.0','1.0','INSTANTIATED',7,'CREATED'),(301,'udm-1.0-instance','vnfInstance-inside-demo',12,'free5gc','udm','1.0.0','1.0','INSTANTIATED',8,'CREATED'),(302,'smf-1.0-instance','vnfInstance-inside-demo',14,'free5gc','smf','1.0.0','1.0','INSTANTIATED',9,'CREATED'),(303,'udr-1.0-instance','vnfInstance-inside-demo',15,'free5gc','udr','1.0.0','1.0','INSTANTIATED',10,'CREATED'),(304,'ausf-1.0-instance','vnfInstance-inside-demo',16,'free5gc','ausf','1.0.0','1.0','INSTANTIATED',11,'CREATED'),(305,'upf-1.0-instance','vnfInstance-inside-demo',17,'free5gc','upf','1.0.0','1.0','INSTANTIATED',12,'CREATED');
/*!40000 ALTER TABLE `VnfInstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deployment_info`
--

DROP TABLE IF EXISTS `deployment_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployment_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL COMMENT 'deployment所属的名称空间',
  `deployment_name` varchar(50) DEFAULT NULL,
  `total_replicas` int(11) DEFAULT '0',
  `ready_replicas` int(11) DEFAULT '0',
  `total_cpu_usage` int(11) DEFAULT '0',
  `total_memory_usage` int(11) DEFAULT '0',
  `total_cpu_request` int(11) DEFAULT '0',
  `total_memory_request` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deployment_info`
--

LOCK TABLES `deployment_info` WRITE;
/*!40000 ALTER TABLE `deployment_info` DISABLE KEYS */;
INSERT INTO `deployment_info` VALUES (18,'default','nfs-storage-nfs-client-provisioner',1,1,10,20,0,0),(29,'kube-system','calico-kube-controllers',1,1,9,68,0,0),(30,'kube-system','coredns',2,1,80,86,700,490),(31,'kube-system','kuboard',1,1,0,11,0,0),(32,'kube-system','metrics-server',1,1,29,62,0,0),(33,'kube-system','monitor-blackbox-exporter',1,1,3,3,200,100),(34,'kube-system','monitor-grafana',1,1,3,14,0,0),(35,'kube-system','monitor-kube-state-metrics',1,1,0,20,0,0),(36,'kube-system','monitor-prometheus',1,1,80,551,400,400),(37,'kubernetes-dashboard','dashboard-metrics-scraper',1,1,6,28,0,0),(38,'kubernetes-dashboard','kubernetes-dashboard',1,1,9,21,0,0),(190,'default','sba-computing-deployment',1,1,0,132,0,0),(191,'default','sba-caching-deployment',1,1,0,42,0,0),(192,'default','sba-information-deployment',1,1,0,36,0,0),(202,'demo','nrf',2,2,58,8,200,200),(203,'demo','amf',2,2,108,8,200,200),(204,'demo','nssf',1,1,0,4,100,100),(205,'demo','pcf',1,1,0,4,100,100),(206,'demo','udm',1,1,0,4,100,100),(207,'demo','smf',2,1,0,9,200,200),(208,'demo','udr',1,1,1,5,100,100),(209,'demo','ausf',1,1,0,10,100,100),(210,'demo','upf',3,2,0,3,300,300);
/*!40000 ALTER TABLE `deployment_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_info`
--

DROP TABLE IF EXISTS `node_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_name` varchar(30) DEFAULT NULL,
  `node_ip` varchar(30) DEFAULT NULL,
  `total_cpu` int(11) DEFAULT '0',
  `total_memory` int(11) DEFAULT '0',
  `used_cpu` int(11) DEFAULT '0',
  `used_memory` int(11) DEFAULT '0',
  `memory_pressure` int(11) DEFAULT '0',
  `disk_pressure` int(11) DEFAULT '0',
  `cpu_pressure` int(11) DEFAULT '0',
  `ready` int(11) DEFAULT '0',
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_info`
--

LOCK TABLES `node_info` WRITE;
/*!40000 ALTER TABLE `node_info` DISABLE KEYS */;
INSERT INTO `node_info` VALUES (13,'k8smec','192.168.10.139',4000,15860,1498,12723,0,0,0,0,0),(14,'node1','192.168.10.201',2000,3821,823,3230,0,0,0,0,0),(15,'node2','192.168.10.162',2000,3821,477,1377,0,0,0,0,0),(16,'node3','192.168.10.55',2000,3821,404,1389,0,0,0,0,0),(17,'ubuntu','192.168.10.160',2000,3818,1988,3036,0,0,0,0,0);
/*!40000 ALTER TABLE `node_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个插件',
  `name` varchar(255) DEFAULT NULL COMMENT '插件的名称',
  `description` varchar(255) DEFAULT NULL COMMENT '插件的描述',
  `working_time` varchar(255) DEFAULT NULL COMMENT '插件生效的时间，instantiate，running',
  `url` varchar(255) DEFAULT NULL COMMENT '插件的url，定位插件的位置（本地或网络路径）',
  `required_service` varchar(255) DEFAULT NULL COMMENT '需求的service名称，逗号分隔',
  `installed` varchar(255) DEFAULT NULL COMMENT '插件是否安装，true,false',
  `effective` varchar(255) DEFAULT NULL COMMENT '插件是否生效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pod_info`
--

DROP TABLE IF EXISTS `pod_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pod_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL COMMENT 'pod所属的名称空间',
  `pod_name` varchar(70) DEFAULT NULL,
  `deploy_on_node_name` varchar(30) DEFAULT NULL,
  `deploy_on_node_ip` varchar(30) DEFAULT NULL,
  `deployment_name` varchar(50) DEFAULT NULL,
  `cpu_request` int(11) DEFAULT '0',
  `memory_request` int(11) DEFAULT '0',
  `cpu_usage` int(11) DEFAULT '0',
  `memory_usage` int(11) DEFAULT '0',
  `phase` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=781 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pod_info`
--

LOCK TABLES `pod_info` WRITE;
/*!40000 ALTER TABLE `pod_info` DISABLE KEYS */;
INSERT INTO `pod_info` VALUES (26,'default','nfs-storage-nfs-client-provisioner-77999df47c-gnf2v','node2','192.168.10.162','nfs-storage-nfs-client-provisioner',NULL,NULL,5,9,'Running'),(41,'kube-system','coredns-6955765f44-dhbk2','node1','192.168.10.201','coredns',100,70,4,14,'Running'),(42,'kube-system','monitor-blackbox-exporter-57cd874997-vnppg','k8smec','192.168.10.139','monitor-blackbox-exporter',100,50,3,3,'Running'),(44,'kube-system','kube-proxy-lk66n','k8smec','192.168.10.139','kube',NULL,NULL,1,15,'Running'),(45,'kube-system','kube-proxy-x8mgl','node2','192.168.10.162','kube',NULL,NULL,20,16,'Running'),(47,'kube-system','kuboard-pv-browser-5bw6f','k8smec','192.168.10.139','kuboard-pv',NULL,NULL,0,1,'Running'),(48,'kube-system','kube-controller-manager-k8smec','k8smec','192.168.10.139','kube-controller',200,NULL,8,16,'Running'),(49,'kube-system','kuboard-pv-browser-r7qrl','node1','192.168.10.201','kuboard-pv',NULL,NULL,0,1,'Running'),(50,'kube-system','etcd-k8smec','k8smec','192.168.10.139','etcd',NULL,NULL,13,53,'Running'),(51,'kube-system','kube-apiserver-k8smec','k8smec','192.168.10.139','kube',250,NULL,74,404,'Running'),(52,'kube-system','metrics-server-78664db96b-ckpwx','node3','192.168.10.55','metrics-server',NULL,NULL,2,19,'Running'),(53,'kube-system','kube-proxy-w6mdw','node1','192.168.10.201','kube',NULL,NULL,4,16,'Running'),(54,'kube-system','calico-node-x4649','node1','192.168.10.201','calico',250,NULL,49,48,'Running'),(56,'kubernetes-dashboard','dashboard-metrics-scraper-b68468655-gds9z','node2','192.168.10.162','dashboard-metrics-scraper',NULL,NULL,1,14,'Running'),(57,'kube-system','calico-node-n4m5t','k8smec','192.168.10.139','calico',250,NULL,34,48,'Running'),(58,'kube-system','calico-kube-controllers-6d4bfc7c57-bpfmp','node3','192.168.10.55','calico-kube-controllers',NULL,NULL,2,20,'Running'),(59,'kube-system','monitor-prometheus-node-exporter-pghxd','k8smec','192.168.10.139','monitor-prometheus-node',NULL,NULL,0,2,'Running'),(60,'kube-system','monitor-prometheus-node-exporter-n299w','node2','192.168.10.162','monitor-prometheus-node',NULL,NULL,0,10,'Running'),(61,'kube-system','calico-node-9p2kg','ubuntu','192.168.10.160','calico',250,NULL,110,55,'Running'),(62,'kube-system','calico-node-cnvlz','node3','192.168.10.55','calico',250,NULL,79,84,'Running'),(63,'kube-system','monitor-grafana-74676d6579-xq4vz','k8smec','192.168.10.139','monitor-grafana',NULL,NULL,3,14,'Running'),(64,'kube-system','calico-node-hglph','node2','192.168.10.162','calico',250,NULL,78,85,'Running'),(65,'kube-system','monitor-kube-state-metrics-84f9dd79c7-j6pv8','node3','192.168.10.55','monitor-kube-state-metrics',NULL,NULL,0,12,'Running'),(66,'kube-system','kuboard-5ffbc8466d-xz84t','k8smec','192.168.10.139','kuboard',NULL,NULL,0,4,'Running'),(67,'kube-system','kube-proxy-cz9lv','ubuntu','192.168.10.160','kube',NULL,NULL,33,17,'Running'),(68,'kube-system','kube-scheduler-k8smec','k8smec','192.168.10.139','kube',100,NULL,15,14,'Running'),(69,'kube-system','kuboard-pv-browser-7nx8f','node2','192.168.10.162','kuboard-pv',NULL,NULL,0,1,'Running'),(70,'kube-system','monitor-prometheus-node-exporter-v2xbc','ubuntu','192.168.10.160','monitor-prometheus-node',NULL,NULL,2078,164,'Running'),(71,'kube-system','monitor-prometheus-node-exporter-287bm','node1','192.168.10.201','monitor-prometheus-node',NULL,NULL,11,20,'Running'),(72,'kube-system','kube-proxy-pzf72','node3','192.168.10.55','kube',NULL,NULL,19,15,'Running'),(73,'kube-system','monitor-prometheus-node-exporter-jpnlm','node3','192.168.10.55','monitor-prometheus-node',NULL,NULL,0,7,'Running'),(74,'kubernetes-dashboard','kubernetes-dashboard-64999dbccd-lwd77','k8smec','192.168.10.139','kubernetes-dashboard',NULL,NULL,4,9,'Running'),(76,'kube-system','kuboard-pv-browser-cls54','ubuntu','192.168.10.160','kuboard-pv',NULL,NULL,0,1,'Running'),(77,'kube-system','kuboard-pv-browser-kbrl9','node3','192.168.10.55','kuboard-pv',NULL,NULL,0,1,'Running'),(79,'default','mongo-0','node1','192.168.10.201','mongo',NULL,NULL,7,66,'Running'),(702,'free5gc-core-v1','smf-855666cd5c-lvlhg',NULL,NULL,NULL,NULL,NULL,0,4,NULL),(703,'free5gc-core-v1','ausf-67b9586884-xvpcb',NULL,NULL,NULL,NULL,NULL,0,10,NULL),(704,'free5gc-core-v1','upf-849f54f97d-kgg2m',NULL,NULL,NULL,NULL,NULL,0,1,NULL),(721,'default','sba-computing-deployment-7cccc95898-pr4qp','node2','192.168.10.162','sba-computing-deployment',NULL,NULL,0,27,'Running'),(722,'kube-system','coredns-6955765f44-qgdbn','k8smec','192.168.10.139','coredns',100,70,3,21,'Running'),(723,'kube-system','coredns-6955765f44-qgdbn','k8smec','192.168.10.139','coredns',100,70,3,21,'Running'),(725,'default','sba-computing-deployment-7cccc95898-bd8fn','node1','192.168.10.201','sba-computing-deployment',NULL,NULL,0,21,'Running'),(726,'kubernetes-dashboard','dashboard-metrics-scraper-b68468655-4mvmj','node1','192.168.10.201','dashboard-metrics-scraper',NULL,NULL,3,7,'Running'),(727,'kube-system','monitor-prometheus-7b889796fd-kwgwx','node2','192.168.10.162','monitor-prometheus',200,200,32,222,'Running'),(728,'kube-system','coredns-6955765f44-f6mlr','node2','192.168.10.162','coredns',100,70,NULL,NULL,'Running'),(729,'default','nfs-storage-nfs-client-provisioner-77999df47c-t9fxc','node2','192.168.10.162','nfs-storage-nfs-client-provisioner',NULL,NULL,5,11,'Running'),(730,'default','nfs-storage-nfs-client-provisioner-77999df47c-wbpfg','ubuntu','192.168.10.160','nfs-storage-nfs-client-provisioner',NULL,NULL,NULL,NULL,'Pending'),(731,'default','sba-caching-deployment-ddd859c8-4m5q6','node1','192.168.10.201','sba-caching-deployment',NULL,NULL,0,17,'Running'),(732,'default','sba-computing-deployment-7cccc95898-pc27c','node1','192.168.10.201','sba-computing-deployment',NULL,NULL,0,49,'Running'),(733,'default','sba-information-deployment-7db96f8-fc9hh','node1','192.168.10.201','sba-information-deployment',NULL,NULL,0,16,'Running'),(734,'kube-system','calico-kube-controllers-6d4bfc7c57-nvhm8','node1','192.168.10.201','calico-kube-controllers',NULL,NULL,4,37,'Running'),(735,'kube-system','coredns-6955765f44-wb24c','node1','192.168.10.201','coredns',100,70,13,16,'Running'),(736,'kube-system','metrics-server-78664db96b-pt8rc','node1','192.168.10.201','metrics-server',NULL,NULL,5,20,'Running'),(737,'kube-system','monitor-kube-state-metrics-84f9dd79c7-8sjxm','node2','192.168.10.162','monitor-kube-state-metrics',NULL,NULL,0,8,'Running'),(738,'kube-system','monitor-prometheus-7b889796fd-4lxfz','ubuntu','192.168.10.160','monitor-prometheus',200,200,48,329,'Running'),(754,'demo','mongo-0',NULL,NULL,'mongo',NULL,NULL,NULL,NULL,'Pending'),(755,'demo','nrf-6c57d578b4-hrttq','node3','192.168.10.55','nrf',100,100,30,4,'Running'),(756,'demo','nrf-6c57d578b4-q49tm','node2','192.168.10.162','nrf',100,100,28,4,'Running'),(757,'demo','amf-7f46f555-tn5b6','node3','192.168.10.55','amf',100,100,55,4,'Running'),(758,'demo','amf-7f46f555-txtwq','node2','192.168.10.162','amf',100,100,53,4,'Running'),(759,'demo','nssf-fb5474976-hps2l','node3','192.168.10.55','nssf',100,100,0,4,'Running'),(760,'demo','pcf-56f75bbbd4-bvp6l','node3','192.168.10.55','pcf',100,100,0,4,'Running'),(761,'demo','udm-6dbf78d69f-sqkcq','node3','192.168.10.55','udm',100,100,0,4,'Running'),(762,'demo','smf-855666cd5c-ljprl','ubuntu','192.168.10.160','smf',100,100,0,5,'Running'),(763,'demo','smf-855666cd5c-hx62p','node3','192.168.10.55','smf',100,100,0,4,'Running'),(764,'demo','udr-749c6b9b67-2nv4f','node3','192.168.10.55','udr',100,100,1,5,'Running'),(765,'demo','ausf-67b9586884-lgdnm','node3','192.168.10.55','ausf',100,100,0,10,'Running'),(766,'demo','upf-849f54f97d-qnfj4','ubuntu','192.168.10.160','upf',100,100,0,1,'Running'),(767,'demo','upf-849f54f97d-cbzfd','node3','192.168.10.55','upf',100,100,0,1,'Running'),(768,'demo','upf-849f54f97d-kcqdz','node2','192.168.10.162','upf',100,100,0,1,'Running'),(769,'kube-system','kuboard-5ffbc8466d-2nhvw','node2','192.168.10.162','kuboard',NULL,NULL,0,7,'Running'),(770,'kube-system','coredns-6955765f44-6qzbj','ubuntu','192.168.10.160','coredns',100,70,57,14,'Running'),(771,'kube-system','monitor-blackbox-exporter-57cd874997-66l8c',NULL,NULL,'monitor-blackbox-exporter',100,50,NULL,NULL,'Pending'),(772,'kube-system','monitor-grafana-74676d6579-jr4hk',NULL,NULL,'monitor-grafana',NULL,NULL,NULL,NULL,'Pending'),(773,'kubernetes-dashboard','kubernetes-dashboard-64999dbccd-x6fkq','ubuntu','192.168.10.160','kubernetes-dashboard',NULL,NULL,5,12,'Running'),(774,'kube-system','calico-kube-controllers-6d4bfc7c57-9j426','node2','192.168.10.162','calico-kube-controllers',NULL,NULL,3,11,'Running'),(775,'default','sba-information-deployment-7db96f8-fn65s','node2','192.168.10.162','sba-information-deployment',NULL,NULL,0,20,'Running'),(776,'default','sba-caching-deployment-ddd859c8-g2rqc','node2','192.168.10.162','sba-caching-deployment',NULL,NULL,0,25,'Running'),(777,'kubernetes-dashboard','dashboard-metrics-scraper-b68468655-p27xz','ubuntu','192.168.10.160','dashboard-metrics-scraper',NULL,NULL,2,7,'Running'),(778,'default','sba-computing-deployment-7cccc95898-nf4l4','node3','192.168.10.55','sba-computing-deployment',NULL,NULL,0,35,'Running'),(779,'kube-system','metrics-server-78664db96b-hrwqp','ubuntu','192.168.10.160','metrics-server',NULL,NULL,22,23,'Running'),(780,'kube-system','coredns-6955765f44-6brf8','node2','192.168.10.162','coredns',100,70,NULL,NULL,'Running');
/*!40000 ALTER TABLE `pod_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_info`
--

DROP TABLE IF EXISTS `service_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识一个Service',
  `namespace` varchar(255) DEFAULT NULL COMMENT 'service所属的名称空间',
  `service_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cluster_ip` varchar(255) DEFAULT NULL,
  `node_port` int(255) DEFAULT NULL,
  `cluster_port` int(255) DEFAULT NULL,
  `container_port` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_info`
--

LOCK TABLES `service_info` WRITE;
/*!40000 ALTER TABLE `service_info` DISABLE KEYS */;
INSERT INTO `service_info` VALUES (1,'default','kubernetes','ClusterIP','10.96.0.1',NULL,443,6443),(14,'kube-system','kube-dns','ClusterIP','10.96.0.10',NULL,53,53),(15,'kube-system','kuboard','NodePort','10.103.241.232',32567,80,80),(16,'kube-system','metrics-server','ClusterIP','10.110.140.122',NULL,443,4443),(17,'kube-system','monitor-blackbox-exporter','ClusterIP','10.108.84.216',NULL,9115,9115),(18,'kube-system','monitor-grafana','ClusterIP','10.109.173.62',NULL,3000,3000),(19,'kube-system','monitor-prometheus','ClusterIP','10.96.148.61',NULL,9090,9090),(20,'kube-system','monitor-prometheus-node-exporter','ClusterIP','None',NULL,9100,9100),(21,'kubernetes-dashboard','dashboard-metrics-scraper','ClusterIP','10.98.12.197',NULL,8000,8000),(22,'kubernetes-dashboard','kubernetes-dashboard','NodePort','10.100.31.172',31104,443,8443),(29,'default','mongo','ClusterIP','None',NULL,27017,27017),(30,'default','mongo-service','NodePort','10.111.4.122',5294,27017,27017),(31,'kube-system','monitor-kube-state-metrics','ClusterIP','10.110.84.150',NULL,8080,NULL),(250,'default','sba-computing-service-name','NodePort','10.99.61.151',30000,5000,5000),(251,'default','sba-caching-service-name','NodePort','10.108.189.171',30002,6000,6000),(252,'default','sba-information-service-name','NodePort','10.100.184.33',30007,7000,7000),(263,'demo','mongo','ClusterIP','None',NULL,27017,27017),(264,'demo','nrf','ClusterIP','10.100.199.251',NULL,29510,29510),(265,'demo','amf','ClusterIP','10.108.209.100',NULL,29518,29518),(266,'demo','nssf','ClusterIP','10.104.165.140',NULL,29531,29531),(267,'demo','pcf','ClusterIP','10.103.145.69',NULL,29507,29507),(268,'demo','udm','ClusterIP','10.99.62.111',NULL,29503,29503),(269,'demo','smf','ClusterIP','10.105.95.55',NULL,29502,29502),(270,'demo','udr','ClusterIP','10.102.129.188',NULL,29504,29504),(271,'demo','ausf','ClusterIP','10.108.222.50',NULL,29509,29509),(272,'demo','upf','ClusterIP','10.107.247.48',NULL,8805,8805);
/*!40000 ALTER TABLE `service_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(50) NOT NULL,
  `file_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES (6,'template.yaml','apiVersion: apps/v1\nkind: Deployment\nmetadata:\n  name: {{ .Values.name }}\n  namespace: {{ .Release.Namespace }}\n  labels:\n    description: {{ .Values.deploymentDescription }}\nspec:\n  replicas: {{ .Values.replicas }}\n  selector:\n    matchLabels:\n      app: {{ .Values.name }}\n  template:\n    metadata:\n      annotations:\n        kubernetes.io/ingress-bandwidth: {{ .Values.bitrateRequirement }}\n        kubernetes.io/egress-bandwidth: {{ .Values.bitrateRequirement }}\n      labels:\n        app: {{ .Values.name }}\n        description: {{ .Values.podDescription }}\n    spec:\n      containers:\n      - name: {{ .Values.name }}\n        image: {{ .Values.imageUrl }}\n        imagePullPolicy: IfNotPresent\n        ports:\n        - name: {{ .Values.portName }}\n          containerPort: {{ .Values.containerPort }}\n          protocol: {{ .Values.l3protocol }}\n        resources:\n          requests:\n            cpu: {{ .Values.requestCpu }}\n            memory: {{ .Values.requestMemory }}\n          limits:\n            cpu: {{ .Values.cpuLimit }}\n            memory: {{ .Values.memoryLimit }}\n        volumeMounts:\n        - name: config\n          mountPath: \"/free5gc/config/free5gc_config\"\n          readOnly: true\n      volumes:\n      - name: config\n        configMap:\n          name: config\n      affinity:\n        nodeAffinity: \n          preferredDuringSchedulingIgnoredDuringExecution:\n          - weight: 1\n            preference:\n              matchExpressions:\n              - key: location\n                operator: In\n                values: \n                - {{ .Values.location }}\n        podAffinity:\n            preferredDuringSchedulingIgnoredDuringExecution:\n            - weight: 1\n              podAffinityTerm:\n                labelSelector:\n                  matchExpressions:\n                  - key: app\n                    operator: {{ .Values.podAffinity }}\n                    values:\n                    - {{ .Values.name }}\n                topologyKey: kubernetes.io/hostname\n---\napiVersion: v1\nkind: Service\nmetadata:\n  name: {{ .Values.name }}\n  namespace: {{ .Release.Namespace }}\nspec:\n  type: ClusterIP\n  ports:\n    - port: {{ .Values.servicePort }}\n      targetPort: {{ .Values.containerPort }}\n      protocol: {{ .Values.l3protocol }}\n      name: {{ .Values.portName }}\n  selector:\n    app: {{ .Values.name }}\n           \n                \n                \n        \n'),(7,'mongo.yaml','apiVersion: v1\nkind: Service\nmetadata:\n  name: mongo\n  namespace: {{ .Release.Namespace }}\nspec:\n  ports:\n  - name: mongo\n    port: 27017\n  clusterIP: None\n  selector:\n    app: mongo\n---\n#apiVersion: v1\n#kind: Service\n#metadata: \n#  name: mongo-service\n#spec: \n#  ports: \n#  - name: mongo-http\n#    port: 27017\n#  selector: \n#    app: mongo\n#  type: NodePort\n---\napiVersion: apps/v1\nkind: StatefulSet\nmetadata: \n  name: mongo\n  namespace: {{ .Release.Namespace }}\nspec: \n  selector: \n    matchLabels: \n      app: mongo\n  serviceName: \"mongo\"\n  replicas: 1\n  podManagementPolicy: Parallel\n  template: \n    metadata: \n      labels: \n        app: mongo\n    spec: \n      terminationGracePeriodSeconds: 10\n      containers: \n      - name: mongo\n        image: 192.168.10.60/ien-free5gc/mongo\n        command: [\"/bin/sh\"]\n        args: [\"-c\",\"mongod --bind_ip_all\"]  \n        ports: \n        - containerPort: 27017\n        volumeMounts: \n        - name: mongo-data\n          mountPath: /data/db\n  volumeClaimTemplates:\n  - metadata:\n      name: mongo-data\n    spec:\n      accessModes:\n      - ReadWriteOnce\n      storageClassName: nfs-storage\n      resources:\n        requests:\n          storage: 250Mi\n'),(8,'configMap.yaml','apiVersion: v1\nkind: ConfigMap\nmetadata:\n  name: config\n  namespace: {{ .Release.Namespace }}\ndata:\n  nrfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: NRF initial local configuration\n    configuration:\n      MongoDBName: \"free5gc\"\n      MongoDBUrl: \"mongodb://mongo\"\n      DefaultServiceIP: \"0.0.0.0\"\n      sbi:\n        scheme: http\n        ipv4Addr: 0.0.0.0\n        port: 29510\n      DefaultPlmnId:\n        mcc: \"208\"\n        mnc: \"93\"\n      serviceNameList:\n        - nnrf-nfm\n        - nnrf-disc\n  amfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: AMF initial local configuration\n    configuration:\n      amfName: AMF\n      ngapIpList:\n        - 0.0.0.0\n      sbi:\n        scheme: http\n        registerIPv4: amf # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29518\n      serviceNameList:\n        - namf-comm\n        - namf-evts\n        - namf-mt\n        - namf-loc\n        - namf-oam\n      servedGuamiList:\n        - plmnId:\n            mcc: 208\n            mnc: 93\n          amfId: cafe00\n      supportTaiList:\n        - plmnId:\n            mcc: 208\n            mnc: 93\n          tac: 1\n      plmnSupportList:\n        - plmnId:\n            mcc: 208\n            mnc: 93\n          snssaiList:\n            - sst: 1\n              sd: 010203\n            - sst: 1\n              sd: 112233\n      supportDnnList:\n        - internet\n      nrfUri: http://nrf:29510\n      security:\n        integrityOrder:\n          - NIA2\n          # - NIA0\n        cipheringOrder:\n          - NEA0\n          # - NEA2\n      networkName:\n        full: free5GC\n        short: free\n      t3502: 720\n      t3512: 3600\n      non3gppDeregistrationTimer: 3240\n  ausfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: AUSF initial local configuration\n    configuration:\n      sbi:\n        scheme: http\n        registerIPv4: ausf # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29509\n      serviceNameList:\n        - nausf-auth\n      nrfUri: http://nrf:29510\n      plmnSupportList:\n        - mcc: 208\n          mnc: 93\n        - mcc: 123\n          mnc: 45\n      groupId: ausfGroup001\n  nssfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: NSSF initial local configuration\n    configuration:\n      nssfName: NSSF\n      sbi:\n        scheme: http\n        registerIPv4: nssf # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29531\n      serviceNameList:\n        - nnssf-nsselection\n        - nnssf-nssaiavailability\n      nrfUri: http://nrf:29510\n      supportedPlmnList:\n        - mcc: 208\n          mnc: 93\n      supportedNssaiInPlmnList:\n        - plmnId:\n            mcc: 208\n            mnc: 93\n          supportedSnssaiList:\n            - sst: 1\n              sd: 010203\n            - sst: 1\n              sd: 112233\n            - sst: 1\n              sd: 3\n            - sst: 2\n              sd: 1\n            - sst: 2\n              sd: 2\n      nsiList:\n        - snssai:\n            sst: 1\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 10\n        - snssai:\n            sst: 1\n            sd: 1\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 11\n        - snssai:\n            sst: 1\n            sd: 2\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 12\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 12\n        - snssai:\n            sst: 1\n            sd: 3\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 13\n        - snssai:\n            sst: 2\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 20\n        - snssai:\n            sst: 2\n            sd: 1\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 21\n        - snssai:\n            sst: 1\n            sd: 010203\n          nsiInformationList:\n            - nrfId: http://nrf:29510/nnrf-nfm/v1/nf-instances\n              nsiId: 22\n      amfSetList:\n        - amfSetId: 1\n          amfList:\n            - ffa2e8d7-3275-49c7-8631-6af1df1d9d26\n            - 0e8831c3-6286-4689-ab27-1e2161e15cb1\n            - a1fba9ba-2e39-4e22-9c74-f749da571d0d\n          nrfAmfSet: http://nrf:8081/nnrf-nfm/v1/nf-instances\n          supportedNssaiAvailabilityData:\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33456\n              supportedSnssaiList:\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 2\n                - sst: 2\n                  sd: 1\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33457\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 2\n        - amfSetId: 2\n          nrfAmfSet: http://nrf:8084/nnrf-nfm/v1/nf-instances\n          supportedNssaiAvailabilityData:\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33456\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 3\n                - sst: 2\n                  sd: 1\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33458\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 2\n      amfList:\n        - nfId: 469de254-2fe5-4ca0-8381-af3f500af77c\n          supportedNssaiAvailabilityData:\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33456\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 2\n                - sst: 2\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33457\n              supportedSnssaiList:\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 2\n        - nfId: fbe604a8-27b2-417e-bd7c-8a7be2691f8d\n          supportedNssaiAvailabilityData:\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33458\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 3\n                - sst: 2\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33459\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 2\n                - sst: 2\n                  sd: 1\n        - nfId: b9e6e2cb-5ce8-4cb6-9173-a266dd9a2f0c\n          supportedNssaiAvailabilityData:\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33456\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 1\n                  sd: 2\n                - sst: 2\n            - tai:\n                plmnId:\n                  mcc: 466\n                  mnc: 92\n                tac: 33458\n              supportedSnssaiList:\n                - sst: 1\n                - sst: 1\n                  sd: 1\n                - sst: 2\n                - sst: 2\n                  sd: 1\n      taList:\n        - tai:\n            plmnId:\n              mcc: 466\n              mnc: 92\n            tac: 33456\n          accessType: 3GPP_ACCESS\n          supportedSnssaiList:\n            - sst: 1\n            - sst: 1\n              sd: 1\n            - sst: 1\n              sd: 2\n            - sst: 2\n        - tai:\n            plmnId:\n              mcc: 466\n              mnc: 92\n            tac: 33457\n          accessType: 3GPP_ACCESS\n          supportedSnssaiList:\n            - sst: 1\n            - sst: 1\n              sd: 1\n            - sst: 1\n              sd: 2\n            - sst: 2\n        - tai:\n            plmnId:\n              mcc: 466\n              mnc: 92\n            tac: 33458\n          accessType: 3GPP_ACCESS\n          supportedSnssaiList:\n            - sst: 1\n            - sst: 1\n              sd: 1\n            - sst: 1\n              sd: 3\n            - sst: 2\n          restrictedSnssaiList:\n            - homePlmnId:\n                mcc: 310\n                mnc: 560\n              sNssaiList:\n                - sst: 1\n                  sd: 3\n        - tai:\n            plmnId:\n              mcc: 466\n              mnc: 92\n            tac: 33459\n          accessType: 3GPP_ACCESS\n          supportedSnssaiList:\n            - sst: 1\n            - sst: 1\n              sd: 1\n            - sst: 2\n            - sst: 2\n              sd: 1\n          restrictedSnssaiList:\n            - homePlmnId:\n                mcc: 310\n                mnc: 560\n              sNssaiList:\n                - sst: 2\n                  sd: 1\n      mappingListFromPlmn:\n        - operatorName: NTT Docomo\n          homePlmnId:\n            mcc: 440\n            mnc: 10\n          mappingOfSnssai:\n            - servingSnssai:\n                sst: 1\n                sd: 1\n              homeSnssai:\n                sst: 1\n                sd: 1\n            - servingSnssai:\n                sst: 1\n                sd: 2\n              homeSnssai:\n                sst: 1\n                sd: 3\n            - servingSnssai:\n                sst: 1\n                sd: 3\n              homeSnssai:\n                sst: 1\n                sd: 4\n            - servingSnssai:\n                sst: 2\n                sd: 1\n              homeSnssai:\n                sst: 2\n                sd: 2\n        - operatorName: AT&T Mobility\n          homePlmnId:\n            mcc: 310\n            mnc: 560\n          mappingOfSnssai:\n            - servingSnssai:\n                sst: 1\n                sd: 1\n              homeSnssai:\n                sst: 1\n                sd: 2\n            - servingSnssai:\n                sst: 1\n                sd: 2\n              homeSnssai:\n                sst: 1\n                sd: 3\n  pcfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: PCF initial local configuration\n    configuration:\n      pcfName: PCF\n      sbi:\n        scheme: http\n        registerIPv4: pcf # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29507\n      timeFormat: 2019-01-02 15:04:05\n      defaultBdtRefId: BdtPolicyId-\n      nrfUri: http://nrf:29510\n      serviceList:\n        - serviceName: npcf-am-policy-control\n        - serviceName: npcf-smpolicycontrol\n          suppFeat: 3fff\n        - serviceName: npcf-bdtpolicycontrol\n        - serviceName: npcf-policyauthorization\n          suppFeat: 3\n        - serviceName: npcf-eventexposure\n        - serviceName: npcf-ue-policy-control  \n  smfcfg.conf: |\n    info:\n      version: 1.0.0\n      description: SMF initial local configuration\n    configuration:\n      smfName: SMF\n      sbi:\n        scheme: http\n        registerIPv4: smf # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29502\n        tls:\n          key: free5gc/support/TLS/smf.key\n          pem: free5gc/support/TLS/smf.pem\n      serviceNameList:\n        - nsmf-pdusession\n        - nsmf-event-exposure\n        - nsmf-oam\n      snssai_info:\n        - sNssai:\n            sst: 1\n            sd: 010203\n          dnnSmfInfoList:\n            - dnn: internet\n        - sNssai:\n            sst: 1\n            sd: 112233\n          dnnSmfInfoList:\n            - dnn: internet\n      pfcp:\n        addr: 0.0.0.0\n      userplane_information:\n        up_nodes:\n          gNB1:\n            type: AN\n            an_ip: 127.0.0.100\n          UPF:\n            type: UPF\n            node_id: 127.0.0.8\n        links:\n          - A: gNB1\n            B: UPF\n      ue_subnet: 60.60.0.0/16\n      dnn:\n        internet:\n          dns:\n            ipv4: 8.8.8.8\n            ipv6: 2001:4860:4860::8888\n        internet2:\n          dns:\n            ipv4: 8.8.4.4\n            ipv6: 2001:4860:4860::8844\n      nrfUri: http://nrf:29510\n  udmcfg.conf: |\n    info:\n      version: 1.0.0\n      description: UDM initial local configuration\n    configuration:\n      serviceNameList:\n        - nudm-sdm\n        - nudm-uecm\n        - nudm-ueau\n        - nudm-ee\n        - nudm-pp\n      sbi:\n        scheme: http\n        registerIPv4: udm # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29503\n        tls:\n          log: free5gc/udmsslkey.log\n          pem: free5gc/support/TLS/udm.pem\n          key: free5gc/support/TLS/udm.key\n      \n      udrclient:\n        scheme: http\n        ipv4Addr: udr\n        port: 29504\n      nrfclient:\n        scheme: http\n        ipv4Addr: nrf\n        port: 29510\n      nrfUri: http://nrf:29510\n      # test data set from TS33501-f60 Annex C.4\n      # udmProfileAHNPublicKey: 5a8d38864820197c3394b92613b20b91633cbd897119273bf8e4a6f4eec0a650\n      # udmProfileAHNPrivateKey: c53c22208b61860b06c62e5406a7b330c2b577aa5558981510d128247d38bd1d\n      # udmProfileBHNPublicKey: 0472DA71976234CE833A6907425867B82E074D44EF907DFB4B3E21C1C2256EBCD15A7DED52FCBB097A4ED250E036C7B9C8C7004C4EEDC4F068CD7BF8D3F900E3B4\n      # udmProfileBHNPrivateKey: F1AB1074477EBCC7F554EA1C5FC368B1616730155E0041AC447D6301975FECDA\n      keys:\n        udmProfileAHNPublicKey: 5a8d38864820197c3394b92613b20b91633cbd897119273bf8e4a6f4eec0a650\n        udmProfileAHNPrivateKey: c53c22208b61860b06c62e5406a7b330c2b577aa5558981510d128247d38bd1d\n        udmProfileBHNPublicKey: 0472DA71976234CE833A6907425867B82E074D44EF907DFB4B3E21C1C2256EBCD15A7DED52FCBB097A4ED250E036C7B9C8C7004C4EEDC4F068CD7BF8D3F900E3B4\n        udmProfileBHNPrivateKey: F1AB1074477EBCC7F554EA1C5FC368B1616730155E0041AC447D6301975FECDA\n  udrcfg.conf: |\n    info:\n      version: 1.0.0\n      description: UDR initial local configuration\n    configuration:\n      sbi:\n        scheme: http\n        registerIPv4: udr # IP used to register to NRF\n        bindingIPv4: 0.0.0.0  # IP used to bind the service\n        port: 29504\n      mongodb:\n        name: free5gc\n        url: mongodb://mongo\n      nrfUri: http://nrf:29510\n  uerouting.yaml: |\n    info:\n      version: 1.0.0\n      description: Routing information for UE\n    ueRoutingInfo:\n      - SUPI: imsi-2089300007487\n        AN: 10.200.200.101\n        PathList:\n          - DestinationIP: 60.60.0.101\n            UPF: !!seq\n              - BranchingUPF\n              - AnchorUPF1\n          - DestinationIP: 60.60.0.103\n            UPF: !!seq\n              - BranchingUPF\n              - AnchorUPF2\n      - SUPI: imsi-2089300007486\n        AN: 10.200.200.102\n        PathList:\n          - DestinationIP: 10.0.0.10\n            UPF: !!seq\n              - BranchingUPF\n              - AnchorUPF1\n          - DestinationIP: 10.0.0.11\n            UPF: !!seq\n              - BranchingUPF\n              - AnchorUPF2\n    routeProfile:\n      MEC1:\n        forwardingPolicyID: 10\n    pfdDataForApp:\n      - applicationId: edge\n        pfds:\n          - pfdID: pfd1\n            flowDescriptions:\n              - permit out ip from 60.60.0.1 8080 to any\n  upfcfg.yaml: |\n    info:\n      version: 1.0.0\n      description: UPF configuration\n    configuration:\n      # debugLevel: panic|fatal|error|warn|info|debug|trace\n      debugLevel: trace\n      pfcp:\n        - addr: 127.0.0.8\n      gtpu:\n        - addr: 127.0.0.8\n        # [optional] gtpu.name\n        # - name: upf.5gc.nctu.me\n        # [optional] gtpu.ifname\n        # - ifname: gtpif\n      dnn_list:\n        - dnn: internet\n          cidr: 60.60.0.0/24\n          # [optional] dnn_list[*].natifname\n          # natifname: eth0\n');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-31 12:41:24
