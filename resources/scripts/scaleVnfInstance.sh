#!/bin/bash
dir=$(pwd)/..

#第一个参数是要调整的副本数目，第二个是release的名称，第三个是chart的名称,第四个是namespace名称
echo "scaling replicas of $2 to $1"
helm upgrade --set replicas=$1 $2 $dir/helmCharts/$3 --namespace $4
