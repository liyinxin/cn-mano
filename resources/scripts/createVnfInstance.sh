#/bin/bash
dir=$(pwd)/..

#入参为chart的名称
echo "creating chart: $1"
helm create $dir/helmCharts/$1
echo "deleting unnecessary templates"
rm $dir/helmCharts/$1/templates/*.yaml
rm $dir/helmCharts/$1/templates/*.txt
rm -rdf $dir/helmCharts/$1/templates/tests
rm -rdf $dir/helmCharts/$1/charts
rm $dir/helmCharts/$1/values.yaml

echo "copying basic templates"
cp $dir/baseTemplate/template.yaml $dir/helmCharts/$1/templates/

echo "vnfInstace create complete" 
