#!/bin/bash
dir=$(pwd)/..

#入参为namespace
#先创建名称空间
kubectl create ns $1
echo "creating ns $1"

#再创建mongodb数据库服务和配置文件configMap，这两个资源只需要创建一次就行
helm create $dir/helmCharts/$1-prerequests
rm $dir/helmCharts/$1-prerequests/templates/*.yaml
rm $dir/helmCharts/$1-prerequests/templates/*.txt
rm -rdf $dir/helmCharts/$1-prerequests/templates/tests
rm -rdf $dir/helmCharts/$1-prerequests/charts
rm $dir/helmCharts/$1-prerequests/values.yaml

cp $dir/baseTemplate/mongo.yaml $dir/helmCharts/$1-prerequests/templates/
cp $dir/baseTemplate/configMap.yaml $dir/helmCharts/$1-prerequests/templates/

#提前在指定namespace下创建mongodb服务和配置文件
helm install $1-prerequests $dir/helmCharts/$1-prerequests --namespace $1
