#!/bin/bash
dir=$(pwd)/..

#第一个参数是values文件的id,第二个参数是helm chart的文件夹名称，第三个参数是helmrelease的名称,第四个参数是namespace
echo "moving values$1.yaml to $2"
mv $dir/generatedValues/values$1.yaml $dir/helmCharts/$2/values.yaml
echo "instantiating vnfinstance $3"
helm install $3 $dir/helmCharts/$2 --namespace $4
