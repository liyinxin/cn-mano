#!/bin/bash

START=`date +%s%N`;
helm upgrade --set replicas=$1 nrf-test nrf-1.0/
END=`date +%s%N`;
time=$((END-START))
time=`expr $time / 1000000`
echo $time
