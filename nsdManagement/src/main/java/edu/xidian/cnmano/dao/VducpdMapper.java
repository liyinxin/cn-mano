package edu.xidian.cnmano.dao;

import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;
import edu.xidian.cnmano.entities.nsdmanagement.VducpdExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface VducpdMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    long countByExample(VducpdExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int deleteByExample(VducpdExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int deleteByPrimaryKey(Integer cpdId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int insert(Vducpd record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int insertSelective(Vducpd record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    List<Vducpd> selectByExample(VducpdExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    Vducpd selectByPrimaryKey(Integer cpdId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int updateByExampleSelective(@Param("record") Vducpd record, @Param("example") VducpdExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int updateByExample(@Param("record") Vducpd record, @Param("example") VducpdExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int updateByPrimaryKeySelective(Vducpd record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VduCpd
     *
     * @mbg.generated Sat Jan 16 16:51:10 CST 2021
     */
    int updateByPrimaryKey(Vducpd record);
}