package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VirtualizednetworkfunctiondeploymentflavourMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Virtualizednetworkfunctiondeploymentflavour;
import edu.xidian.cnmano.entities.nsdmanagement.VirtualizednetworkfunctiondeploymentflavourExample;
import edu.xidian.cnmano.entities.nstransform.VduprofileFull;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondeploymentflavourFull;
import edu.xidian.cnmano.service.VduProfileService;
import edu.xidian.cnmano.service.VnfdfService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:07
 */
@Service
public class VnfdfServiceImpl implements VnfdfService {
    @Resource
    private VirtualizednetworkfunctiondeploymentflavourMapper mapper;

    @Resource
    private VduProfileService vduProfileService;

    @Override
    public List<Virtualizednetworkfunctiondeploymentflavour> listVnfdf() {
        VirtualizednetworkfunctiondeploymentflavourExample example = new VirtualizednetworkfunctiondeploymentflavourExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createVnfdf(Virtualizednetworkfunctiondeploymentflavour vnfdf) {
        return mapper.insert(vnfdf);
    }

    @Override
    public int deleteVnfdf(int flavourId) {
        return mapper.deleteByPrimaryKey(flavourId);
    }

    @Override
    public VirtualizednetworkfunctiondeploymentflavourFull getVnfdfFullById(Integer flavourId) {
        Virtualizednetworkfunctiondeploymentflavour vnfdf = mapper.selectByPrimaryKey(flavourId);
        VirtualizednetworkfunctiondeploymentflavourFull vnfdfFull = new VirtualizednetworkfunctiondeploymentflavourFull();
        vnfdfFull.setFlavourId(flavourId);
        vnfdfFull.setDescription(vnfdf.getDescription());
        String[] vduProfiles = vnfdf.getVduProfile().split(",");
        VduprofileFull vduprofileFull = vduProfileService.getVduprofileFullById(Integer.parseInt(vduProfiles[0]));
        List<VduprofileFull> vduprofileFullList = new ArrayList<>();
        vduprofileFullList.add(vduprofileFull);
        vnfdfFull.setVduProfile(vduprofileFullList);
        return vnfdfFull;
    }

    @Override
    public Virtualizednetworkfunctiondeploymentflavour getVnfdfById(Integer vnfdfId) {
        return mapper.selectByPrimaryKey(vnfdfId);
    }
}
