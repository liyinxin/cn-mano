package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualdeploymentunit;
import edu.xidian.cnmano.entities.nstransform.VirtualdeploymentunitFull;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:01
 */
public interface VduService {
    List<Virtualdeploymentunit> listVdu();

    int createVdu(Virtualdeploymentunit vdu);

    int deleteVdu(int vduId);

    VirtualdeploymentunitFull getVduFullById(Integer vduId);

    Virtualdeploymentunit getVduById(Integer vduId);
}
