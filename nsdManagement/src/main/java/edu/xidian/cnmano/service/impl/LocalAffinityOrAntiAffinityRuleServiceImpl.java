package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.LocalaffinityorantiaffinityruleMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Localaffinityorantiaffinityrule;
import edu.xidian.cnmano.entities.nsdmanagement.LocalaffinityorantiaffinityruleExample;
import edu.xidian.cnmano.service.LocalAffinityOrAntiAffinityRuleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:46
 */
@Service
public class LocalAffinityOrAntiAffinityRuleServiceImpl implements LocalAffinityOrAntiAffinityRuleService {

    @Resource
    LocalaffinityorantiaffinityruleMapper mapper;

    @Override
    public List<Localaffinityorantiaffinityrule> listLocalAffinity() {
        LocalaffinityorantiaffinityruleExample example = new LocalaffinityorantiaffinityruleExample();
        return mapper.selectByExample(example);
    }

    @Override
    public Localaffinityorantiaffinityrule getLocalAffinityById(int ruleIds) {
        return mapper.selectByPrimaryKey(ruleIds);
    }
}
