package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VirtualcpdMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Virtualcpd;
import edu.xidian.cnmano.entities.nsdmanagement.VirtualcpdExample;
import edu.xidian.cnmano.service.VirtualCpdService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:07
 */
@Service
public class VirtualCpdServiceImpl implements VirtualCpdService {
    @Resource
    private VirtualcpdMapper mapper;

    @Override
    public List<Virtualcpd> listVirtualCpd() {
        VirtualcpdExample example = new VirtualcpdExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createVirtualcpd(Virtualcpd virtualcpd) {
        return mapper.insert(virtualcpd);
    }

    @Override
    public int deleteVirtualcpd(int cpdId) {
        return mapper.deleteByPrimaryKey(cpdId);
    }

    @Override
    public Virtualcpd getVirtualCpdById(int cpdId) {
        return mapper.selectByPrimaryKey(cpdId);
    }
}
