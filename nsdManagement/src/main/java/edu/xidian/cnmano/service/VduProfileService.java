package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Vduprofile;
import edu.xidian.cnmano.entities.nstransform.VduprofileFull;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:01
 */
public interface VduProfileService {
    List<Vduprofile> listVduProfile();

    int createVduprofile(Vduprofile vduprofile);

    int deleteVduprofile(int vduProfileId);

    VduprofileFull getVduprofileFullById(int vduProfileId);

    Vduprofile getVduprofileById(Integer vduProfileId);
}
