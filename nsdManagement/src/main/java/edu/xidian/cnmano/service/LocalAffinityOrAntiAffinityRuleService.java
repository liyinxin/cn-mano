package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Localaffinityorantiaffinityrule;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:46
 */
public interface LocalAffinityOrAntiAffinityRuleService {
    List<Localaffinityorantiaffinityrule> listLocalAffinity();

    Localaffinityorantiaffinityrule getLocalAffinityById(int ruleIds);
}
