package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VirtualizednetworkfunctiondescriptorMapper;
import edu.xidian.cnmano.entities.nsdmanagement.*;
import edu.xidian.cnmano.entities.nstransform.VirtualdeploymentunitFull;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondescriptorFull;
import edu.xidian.cnmano.service.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:08
 */
@Service
public class VnfdServiceImpl implements VnfdService {
    @Resource
    private VirtualizednetworkfunctiondescriptorMapper mapper;

    @Resource
    private VduService vduService;

    @Resource
    private VirtualCpdService virtualCpdService;

    @Resource
    private OsContainerDescService osContainerDescService;

    @Resource
    private VnfdfService vnfdfService;

    @Resource
    private VduProfileService vduProfileService;

    @Override
    public List<Virtualizednetworkfunctiondescriptor> listVnfd() {
        VirtualizednetworkfunctiondescriptorExample example = new VirtualizednetworkfunctiondescriptorExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createNsd(Virtualizednetworkfunctiondescriptor vnfd) {
        return mapper.insert(vnfd);
    }

    @Override
    public int deleteNsd(int vnfdId) {
        return mapper.deleteByPrimaryKey(vnfdId);
    }

    @Override
    public Virtualizednetworkfunctiondescriptor getVnfdById(Integer vnfdId) {
        return mapper.selectByPrimaryKey(vnfdId);
    }

    @Override
    public VirtualizednetworkfunctiondescriptorFull getVnfdFullById(Integer vnfdId) {
        Virtualizednetworkfunctiondescriptor vnfd = mapper.selectByPrimaryKey(vnfdId);
        VirtualizednetworkfunctiondescriptorFull vnfdFull = new VirtualizednetworkfunctiondescriptorFull();
        vnfdFull.setVnfdId(vnfd.getVnfdId());
        vnfdFull.setVnfProvider(vnfd.getVnfProvider());
        vnfdFull.setVnfProductName(vnfd.getVnfProductName());
        vnfdFull.setVnfSoftwareVersion(vnfd.getVnfSoftwareVersion());
        vnfdFull.setVnfdVersion(vnfd.getVnfdVersion());
        vnfdFull.setDeploymentFlavour(vnfd.getDeploymentFlavour());
        String[] vdus = vnfd.getVdu().split(",");
        VirtualdeploymentunitFull vduFull = vduService.getVduFullById(Integer.parseInt(vdus[0]));
        List<VirtualdeploymentunitFull> vduFullList = new ArrayList<>();
        vduFullList.add(vduFull);
        vnfdFull.setVdu(vduFullList);
        String[] virtualCpdIds = vnfd.getVirtualCpd().split(",");
        Virtualcpd virtualCpd = virtualCpdService.getVirtualCpdById(Integer.parseInt(virtualCpdIds[0]));
        List<Virtualcpd> virtualCpdList = new ArrayList<>();
        virtualCpdList.add(virtualCpd);
        vnfdFull.setVirtualCpd(virtualCpdList);
        return vnfdFull;
    }

    @Override
    public Oscontainerdesc getOscontainerdescByVnfdId(Integer vnfdId) {
        Integer vduId = Integer.parseInt(mapper.selectByPrimaryKey(vnfdId).getVdu().split(",")[0]);
        Integer osContainerDescId = Integer.parseInt(vduService.getVduById(vduId).getOsContainerDesc().split(",")[0]);
        return osContainerDescService.getOscontainerdescById(osContainerDescId);
    }

    @Override
    public Vduprofile getVduprofileByVnfdId(Integer vnfdId) {
        Integer vnfdfId = mapper.selectByPrimaryKey(vnfdId).getDeploymentFlavour();
        Integer vduProfileId = Integer.parseInt(vnfdfService.getVnfdfById(vnfdfId).getVduProfile().split(",")[0]);
        return vduProfileService.getVduprofileById(vduProfileId);
    }
}
