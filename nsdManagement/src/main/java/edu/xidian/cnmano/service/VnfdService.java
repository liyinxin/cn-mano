package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nsdmanagement.Vduprofile;
import edu.xidian.cnmano.entities.nsdmanagement.Virtualizednetworkfunctiondescriptor;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondescriptorFull;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:02
 */
public interface VnfdService {
    List<Virtualizednetworkfunctiondescriptor> listVnfd();

    int createNsd(Virtualizednetworkfunctiondescriptor vnfd);

    int deleteNsd(int vnfdId);

    Virtualizednetworkfunctiondescriptor getVnfdById(Integer vnfdId);

    VirtualizednetworkfunctiondescriptorFull getVnfdFullById(Integer vnfdId);

    Oscontainerdesc getOscontainerdescByVnfdId(Integer vnfdId);

    Vduprofile getVduprofileByVnfdId(Integer vnfdId);
}
