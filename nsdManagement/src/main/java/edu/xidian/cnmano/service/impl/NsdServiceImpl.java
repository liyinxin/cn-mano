package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.NetworkservicedescriptorMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Networkservicedescriptor;
import edu.xidian.cnmano.entities.nsdmanagement.NetworkservicedescriptorExample;
import edu.xidian.cnmano.service.NsdService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-13:29
 */
@Service
public class NsdServiceImpl implements NsdService {

    @Resource
    private NetworkservicedescriptorMapper nsdMapper;

    @Override
    public List<Networkservicedescriptor> listNsd() {
        NetworkservicedescriptorExample example = new NetworkservicedescriptorExample();
        return nsdMapper.selectByExample(example);
    }

    @Override
    public int createNsd(Networkservicedescriptor nsd) {
        return nsdMapper.insert(nsd);
    }

    @Override
    public int deleteNsd(int nsdId) {
        return nsdMapper.deleteByPrimaryKey(nsdId);
    }

    @Override
    public Networkservicedescriptor getNsdById(Integer nsdId) {
        return nsdMapper.selectByPrimaryKey(nsdId);
    }
}
