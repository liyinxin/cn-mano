package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VducpdMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;
import edu.xidian.cnmano.entities.nsdmanagement.VducpdExample;
import edu.xidian.cnmano.service.VduCpdService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:58
 */
@Service
public class VduCpdServiceImpl implements VduCpdService {

    @Resource
    VducpdMapper mapper;

    @Override
    public List<Vducpd> listVducpd() {
        VducpdExample example = new VducpdExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createVducpd(Vducpd vducpd) {
        return mapper.insert(vducpd);
    }

    @Override
    public int deleteVducpd(int vducpdId) {
        return mapper.deleteByPrimaryKey(vducpdId);
    }

    @Override
    public Vducpd getVducpdById(int cpdId) {
        return mapper.selectByPrimaryKey(cpdId);
    }
}
