package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.AffinityorantiaffinitygroupMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Affinityorantiaffinitygroup;
import edu.xidian.cnmano.entities.nsdmanagement.AffinityorantiaffinitygroupExample;
import edu.xidian.cnmano.service.AffinityOrAntiAffinityGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:45
 */
@Service
public class AffinityOrAntiAffinityGroupServiceImpl implements AffinityOrAntiAffinityGroupService {

    @Resource
    AffinityorantiaffinitygroupMapper mapper;

    @Override
    public List<Affinityorantiaffinitygroup> listAffinityGroup() {
        AffinityorantiaffinitygroupExample example = new AffinityorantiaffinitygroupExample();
        return mapper.selectByExample(example);
    }

    @Override
    public Affinityorantiaffinitygroup getAffinityGroupById(int groupId) {
        return mapper.selectByPrimaryKey(groupId);
    }
}
