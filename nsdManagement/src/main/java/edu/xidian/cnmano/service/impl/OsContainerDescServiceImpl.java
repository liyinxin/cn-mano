package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.OscontainerdescMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nsdmanagement.OscontainerdescExample;
import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;
import edu.xidian.cnmano.entities.nstransform.OscontainerdescFull;
import edu.xidian.cnmano.service.OsContainerDescService;
import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:57
 */
@Service
public class OsContainerDescServiceImpl implements OsContainerDescService {

    @Resource
    OscontainerdescMapper mapper;

    @Override
    public List<Oscontainerdesc> listOscontainerdesc() {
        OscontainerdescExample example = new OscontainerdescExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createOscontainerdesc(Oscontainerdesc oscontainerdesc) {
        return mapper.insert(oscontainerdesc);
    }

    @Override
    public int deleteOscontainerdesc(int osContainerDescId) {
        return mapper.deleteByPrimaryKey(osContainerDescId);
    }

    @Override
    public OscontainerdescFull getOscontainerdescFullById(int osContainerDescId) {
        Oscontainerdesc oscontainerdesc = mapper.selectByPrimaryKey(osContainerDescId);
        OscontainerdescFull oscontainerdescFull = new OscontainerdescFull();
        oscontainerdescFull.setOsContainerDescId(osContainerDescId);
        oscontainerdescFull.setRequestedCpuResources(oscontainerdesc.getRequestedCpuResources());
        oscontainerdescFull.setRequestedMemoryResources(oscontainerdesc.getRequestedMemoryResources());
        oscontainerdescFull.setCpuResourceLimit(oscontainerdesc.getCpuResourceLimit());
        oscontainerdescFull.setMemoryResourceLimit(oscontainerdesc.getMemoryResourceLimit());
        Swimagedesc swimagedesc = RestTemplateUtil.doGetSwimageById(oscontainerdesc.getSwImageDesc());
        oscontainerdescFull.setSwImageDesc(swimagedesc);
        return oscontainerdescFull;
    }

    @Override
    public Oscontainerdesc getOscontainerdescById(Integer osContainerDescId) {
        return mapper.selectByPrimaryKey(osContainerDescId);
    }

}
