package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:58
 */
public interface VduCpdService {

    List<Vducpd> listVducpd();

    int createVducpd(Vducpd vducpd);

    int deleteVducpd(int vducpdId);

    Vducpd getVducpdById(int parseInt);

}
