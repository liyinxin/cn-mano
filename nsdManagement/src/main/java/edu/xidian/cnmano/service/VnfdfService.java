package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualizednetworkfunctiondeploymentflavour;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondeploymentflavourFull;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:03
 */
public interface VnfdfService {
    List<Virtualizednetworkfunctiondeploymentflavour> listVnfdf();

    int createVnfdf(Virtualizednetworkfunctiondeploymentflavour vnfdf);

    int deleteVnfdf(int flavourId);

    VirtualizednetworkfunctiondeploymentflavourFull getVnfdfFullById(Integer flavourId);

    Virtualizednetworkfunctiondeploymentflavour getVnfdfById(Integer vnfdfId);
}
