package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Networkservicedescriptor;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:01
 */
public interface NsdService {
    List<Networkservicedescriptor> listNsd();

    int createNsd(Networkservicedescriptor nsd);

    int deleteNsd(int nsdId);

    Networkservicedescriptor getNsdById(Integer nsdId);
}
