package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nstransform.OscontainerdescFull;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:57
 */
public interface OsContainerDescService {

    List<Oscontainerdesc> listOscontainerdesc();

    int createOscontainerdesc(Oscontainerdesc oscontainerdesc);

    int deleteOscontainerdesc(int osContainerDescId);

    OscontainerdescFull getOscontainerdescFullById(int parseInt);

    Oscontainerdesc getOscontainerdescById(Integer osContainerDescId);
}
