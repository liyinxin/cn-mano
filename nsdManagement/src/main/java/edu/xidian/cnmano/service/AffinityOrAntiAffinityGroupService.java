package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Affinityorantiaffinitygroup;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:45
 */
public interface AffinityOrAntiAffinityGroupService {
    List<Affinityorantiaffinitygroup> listAffinityGroup();

    Affinityorantiaffinitygroup getAffinityGroupById(int groupId);
}
