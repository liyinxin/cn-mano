package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VduprofileMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Affinityorantiaffinitygroup;
import edu.xidian.cnmano.entities.nsdmanagement.Localaffinityorantiaffinityrule;
import edu.xidian.cnmano.entities.nsdmanagement.Vduprofile;
import edu.xidian.cnmano.entities.nsdmanagement.VduprofileExample;
import edu.xidian.cnmano.entities.nstransform.VduprofileFull;
import edu.xidian.cnmano.service.AffinityOrAntiAffinityGroupService;
import edu.xidian.cnmano.service.LocalAffinityOrAntiAffinityRuleService;
import edu.xidian.cnmano.service.VduProfileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:06
 */
@Service
public class VduProfileServiceImpl implements VduProfileService {
    @Resource
    private VduprofileMapper mapper;

    @Resource
    private AffinityOrAntiAffinityGroupService affinityOrAntiAffinityGroupService;

    @Resource
    private LocalAffinityOrAntiAffinityRuleService localAffinityOrAntiAffinityRuleService;

    @Override
    public List<Vduprofile> listVduProfile() {
        VduprofileExample example = new VduprofileExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createVduprofile(Vduprofile vduprofile) {
        return mapper.insert(vduprofile);
    }

    @Override
    public int deleteVduprofile(int vduProfileId) {
        return mapper.deleteByPrimaryKey(vduProfileId);
    }

    @Override
    public VduprofileFull getVduprofileFullById(int vduProfileId) {
        Vduprofile vduprofile = mapper.selectByPrimaryKey(vduProfileId);
        VduprofileFull vduprofileFull = new VduprofileFull();
        vduprofileFull.setVduProfileId(vduProfileId);
        vduprofileFull.setVduId(vduprofile.getVduId());
        vduprofileFull.setMinNumberOfInstances(vduprofile.getMinNumberOfInstances());
        vduprofileFull.setMaxNumberOfInstances(vduprofile.getMaxNumberOfInstances());
        String[] groupIds = vduprofile.getAffinityOrAntiAffinityGroupId().split(",");
        Affinityorantiaffinitygroup affinityGroup = affinityOrAntiAffinityGroupService.getAffinityGroupById(Integer.parseInt(groupIds[0]));
        List<Affinityorantiaffinitygroup> affinityGroupList = new ArrayList<>();
        affinityGroupList.add(affinityGroup);
        vduprofileFull.setAffinityOrAntiAffinityGroup(affinityGroupList);
        String[] ruleIds = vduprofile.getLocalAffinityOrAntiAffinityRuleId().split(",");
        Localaffinityorantiaffinityrule localAffinity = localAffinityOrAntiAffinityRuleService.getLocalAffinityById(Integer.parseInt(ruleIds[0]));
        List<Localaffinityorantiaffinityrule> localAffinityList = new ArrayList<>();
        localAffinityList.add(localAffinity);
        vduprofileFull.setLocalAffinityOrAntiAffinityRule(localAffinityList);
        return vduprofileFull;
    }

    @Override
    public Vduprofile getVduprofileById(Integer vduProfileId) {
        return mapper.selectByPrimaryKey(vduProfileId);
    }
}
