package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualcpd;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:02
 */
public interface VirtualCpdService {
    List<Virtualcpd> listVirtualCpd();

    int createVirtualcpd(Virtualcpd virtualcpd);

    int deleteVirtualcpd(int cpdId);

    Virtualcpd getVirtualCpdById(int cpdId);
}
