package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.VirtualdeploymentunitMapper;
import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;
import edu.xidian.cnmano.entities.nsdmanagement.Virtualdeploymentunit;
import edu.xidian.cnmano.entities.nsdmanagement.VirtualdeploymentunitExample;
import edu.xidian.cnmano.entities.nstransform.OscontainerdescFull;
import edu.xidian.cnmano.entities.nstransform.VirtualdeploymentunitFull;
import edu.xidian.cnmano.service.OsContainerDescService;
import edu.xidian.cnmano.service.VduCpdService;
import edu.xidian.cnmano.service.VduService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-15:07
 */
@Service
public class VduServiceImpl implements VduService {
    @Resource
    private VirtualdeploymentunitMapper mapper;

    @Resource
    private VduCpdService vduCpdService;

    @Resource
    private OsContainerDescService osContainerDescService;

    @Override
    public List<Virtualdeploymentunit> listVdu() {
        VirtualdeploymentunitExample example = new VirtualdeploymentunitExample();
        return mapper.selectByExample(example);
    }

    @Override
    public int createVdu(Virtualdeploymentunit vdu) {
        return mapper.insert(vdu);
    }

    @Override
    public int deleteVdu(int vduId) {
        return mapper.deleteByPrimaryKey(vduId);
    }

    @Override
    public VirtualdeploymentunitFull getVduFullById(Integer vduId) {
        Virtualdeploymentunit vdu = mapper.selectByPrimaryKey(vduId);
        VirtualdeploymentunitFull vduFull = new VirtualdeploymentunitFull();
        vduFull.setVduId(vdu.getVduId());
        vduFull.setName(vdu.getName());
        vduFull.setDescription(vdu.getDescription());
        String[] vduCpds = vdu.getIntCpd().split(",");
        Vducpd vducpd = vduCpdService.getVducpdById(Integer.parseInt(vduCpds[0]));
        List<Vducpd> VducpdList= new ArrayList<>();
        VducpdList.add(vducpd);
        vduFull.setIntCpd(VducpdList);
        String[] osContainerDescIds = vdu.getOsContainerDesc().split(",");
        OscontainerdescFull oscontainerdescFull = osContainerDescService.getOscontainerdescFullById(Integer.parseInt(osContainerDescIds[0]));
        List<OscontainerdescFull> oscontainerdescFullList = new ArrayList<>();
        oscontainerdescFullList.add(oscontainerdescFull);
        vduFull.setOsContainerDesc(oscontainerdescFullList);
        return vduFull;
    }

    @Override
    public Virtualdeploymentunit getVduById(Integer vduId) {
        return mapper.selectByPrimaryKey(vduId);
    }
}
