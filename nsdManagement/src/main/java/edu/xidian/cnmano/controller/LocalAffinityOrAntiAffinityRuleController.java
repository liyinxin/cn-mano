package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Localaffinityorantiaffinityrule;
import edu.xidian.cnmano.service.LocalAffinityOrAntiAffinityRuleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:44
 */
@RestController
@RequestMapping("/affinityrule")
public class LocalAffinityOrAntiAffinityRuleController {

    @Resource
    LocalAffinityOrAntiAffinityRuleService localAffinityOrAntiAffinityRuleService;

    @RequestMapping("")
    public List<Localaffinityorantiaffinityrule> listLocalAffinity(){
        return localAffinityOrAntiAffinityRuleService.listLocalAffinity();
    }

}
