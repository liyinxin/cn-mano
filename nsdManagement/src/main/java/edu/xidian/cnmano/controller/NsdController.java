package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Networkservicedescriptor;
import edu.xidian.cnmano.service.NsdService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author zhr
 * @date 2021/1/12-11:44
 */
@RestController
@RequestMapping("/nsd")
public class NsdController {

    @Resource
    private NsdService nsdService;

    @RequestMapping("")
    public List<Networkservicedescriptor> listNsd(){
        return nsdService.listNsd();
    }

    @RequestMapping("/{nsdId}")
    public Networkservicedescriptor getNsdById(@PathVariable("nsdId") Integer nsdId){
        return nsdService.getNsdById(nsdId);
    }

    @RequestMapping("/create")
    public int createNsd(Networkservicedescriptor nsd){
        int nsdId = nsdService.createNsd(nsd);

        System.out.println("created nsd, id="+ nsdId);
        return nsdId;
    }

    @RequestMapping("/delete")
    public int deleteNsd(@RequestParam("nsdId") int nsdId){
        System.out.println("deleted nsd, id="+ nsdId);
        return nsdService.deleteNsd(nsdId);
    }
}
