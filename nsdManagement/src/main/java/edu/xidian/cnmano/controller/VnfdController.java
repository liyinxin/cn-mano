package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.entities.nsdmanagement.Vduprofile;
import edu.xidian.cnmano.entities.nsdmanagement.Virtualizednetworkfunctiondescriptor;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondescriptorFull;
import edu.xidian.cnmano.service.VduProfileService;
import edu.xidian.cnmano.service.VduService;
import edu.xidian.cnmano.service.VnfdService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:57
 */
@RestController
@RequestMapping("/vnfd")
public class VnfdController {
    @Resource
    private VnfdService vnfdService;

    @Resource
    private VduService vduService;

    @RequestMapping("")
    public List<Virtualizednetworkfunctiondescriptor> listVnfd(){
        return vnfdService.listVnfd();
    }

    @RequestMapping("/{vnfdId}")
    public Virtualizednetworkfunctiondescriptor getVnfdById(@PathVariable("vnfdId") Integer vnfdId){
        return vnfdService.getVnfdById(vnfdId);
    }

    /**
     * 根据vnfdId从数据库获取一个完整的vnfd对象，该对象中的
     * vdu不再是id，而是一个实际vduFull对象
     * @return
     */
    @RequestMapping("/{vnfdId}/full")
    public VirtualizednetworkfunctiondescriptorFull getVnfdFullById(@PathVariable("vnfdId") Integer vnfdId){
        return vnfdService.getVnfdFullById(vnfdId);
    }

    @RequestMapping("/create")
    public int createVnfd(Virtualizednetworkfunctiondescriptor vnfd){
        return vnfdService.createNsd(vnfd);
    }

    @RequestMapping("/delete")
    public int deleteVnfd(@RequestParam("vnfdId") int vnfdId){
        return vnfdService.deleteNsd(vnfdId);
    }

    @RequestMapping("/{vnfdId}/getOscontainerdesc")
    public Oscontainerdesc getOscontainerdescByVnfdId(@PathVariable("vnfdId") Integer vnfdId){
        return vnfdService.getOscontainerdescByVnfdId(vnfdId);
    }

    @RequestMapping("/{vnfdId}/getVduprofile")
    public Vduprofile getVduprofileByVnfdId(@PathVariable("vnfdId") Integer vnfdId){
        return vnfdService.getVduprofileByVnfdId(vnfdId);
    }


}
