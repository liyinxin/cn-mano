package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Vduprofile;
import edu.xidian.cnmano.service.VduProfileService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:59
 */
@RestController
@RequestMapping("/vdup")
public class VduProfileController {
    @Resource
    private VduProfileService vduProfileService;

    @RequestMapping("")
    public List<Vduprofile> listVduProfile(){
        return vduProfileService.listVduProfile();
    }

    @RequestMapping("/create")
    public int createVduprofile(Vduprofile vduprofile){
        return vduProfileService.createVduprofile(vduprofile);
    }

    @RequestMapping("/delete")
    public int deleteVduprofile(@RequestParam("vduProfileId") int vduProfileId){
        return vduProfileService.deleteVduprofile(vduProfileId);
    }
}
