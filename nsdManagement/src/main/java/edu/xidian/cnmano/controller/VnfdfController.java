package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualizednetworkfunctiondeploymentflavour;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondeploymentflavourFull;
import edu.xidian.cnmano.service.VnfdfService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:57
 */
@RestController
@RequestMapping("/vnfdf")
public class VnfdfController {
    @Resource
    private VnfdfService vnfdfService;

    @RequestMapping("")
    public List<Virtualizednetworkfunctiondeploymentflavour> listVnfdf(){
        return vnfdfService.listVnfdf();
    }

    @RequestMapping("/{flavourId}/full")
    public VirtualizednetworkfunctiondeploymentflavourFull getVnfdfFullById(@PathVariable("flavourId") Integer flavourId){
        return vnfdfService.getVnfdfFullById(flavourId);
    }

    @RequestMapping("/create")
    public int createVnfdf(Virtualizednetworkfunctiondeploymentflavour vnfdf){
        return vnfdfService.createVnfdf(vnfdf);
    }

    @RequestMapping("/delete")
    public int deleteVnfdf(@RequestParam("flavourId") int flavourId){
        return vnfdfService.deleteVnfdf(flavourId);
    }
}
