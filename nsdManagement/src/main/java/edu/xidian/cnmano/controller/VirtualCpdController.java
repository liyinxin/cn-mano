package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualcpd;
import edu.xidian.cnmano.service.VirtualCpdService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:59
 */
@RestController
@RequestMapping("/vcpd")
public class VirtualCpdController {
    @Resource
    private VirtualCpdService virtualCpdService;

    @RequestMapping("")
    public List<Virtualcpd> listVirtualCpd(){
        return virtualCpdService.listVirtualCpd();
    }

    @RequestMapping("/create")
    public int createVirtualcpd(Virtualcpd virtualcpd){
        return virtualCpdService.createVirtualcpd(virtualcpd);
    }

    @RequestMapping("/delete")
    public int deleteVirtualcpd(@RequestParam("cpdId") int cpdId){
        return virtualCpdService.deleteVirtualcpd(cpdId);
    }
}
