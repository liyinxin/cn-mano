package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualdeploymentunit;
import edu.xidian.cnmano.service.VduService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:57
 */
@RestController
@RequestMapping("/vdu")
public class VduController {
    @Resource
    private VduService vduService;

    @RequestMapping("")
    public List<Virtualdeploymentunit> listVdu(){
        return vduService.listVdu();
    }

    @RequestMapping("/create")
    public int createVdu(Virtualdeploymentunit vdu){
        return vduService.createVdu(vdu);
    }

    @RequestMapping("/delete")
    public int deleteVdu(@RequestParam("vduId") int vduId){
        return vduService.deleteVdu(vduId);
    }
}
