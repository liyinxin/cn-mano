package edu.xidian.cnmano.controller;
import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;
import edu.xidian.cnmano.service.VduCpdService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:57
 */
@RestController
@RequestMapping("vducpd")
public class VduCpdController {

    @Resource
    VduCpdService vduCpdService;

    @RequestMapping("")
    public List<Vducpd> listVduCpd(){
        return vduCpdService.listVducpd();
    }

    @RequestMapping("/create")
    public int createVducpd(Vducpd vducpd){
        return vduCpdService.createVducpd(vducpd);
    }

    @RequestMapping("/delete")
    public int deleteVducpd(@RequestParam("cpdId") int cpdId){
        return vduCpdService.deleteVducpd(cpdId);
    }
}
