package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Affinityorantiaffinitygroup;
import edu.xidian.cnmano.service.AffinityOrAntiAffinityGroupService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/15-9:43
 */
@RestController
@RequestMapping("/affinitygroup")
public class AffinityOrAntiAffinityGroupController {

    @Resource
    AffinityOrAntiAffinityGroupService affinityOrAntiAffinityGroupService;

    @RequestMapping("")
    public List<Affinityorantiaffinitygroup> listAffinityGroup(){
        return affinityOrAntiAffinityGroupService.listAffinityGroup();
    }
}
