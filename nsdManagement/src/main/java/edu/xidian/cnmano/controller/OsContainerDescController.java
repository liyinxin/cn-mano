package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Oscontainerdesc;
import edu.xidian.cnmano.service.OsContainerDescService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/14-20:56
 */
@RestController
@RequestMapping("/oscontainerdesc")
public class OsContainerDescController {

    @Resource
    OsContainerDescService osContainerDescService;

    @RequestMapping("")
    public List<Oscontainerdesc> listOscontainerdesc(){
        return osContainerDescService.listOscontainerdesc();
    }

    @RequestMapping("/create")
    public int createOscontainerdesc(Oscontainerdesc oscontainerdesc){
        return osContainerDescService.createOscontainerdesc(oscontainerdesc);
    }

    @RequestMapping("/delete")
    public int deleteOscontainerdesc(@RequestParam("osContainerDescId") int osContainerDescId){
        return osContainerDescService.deleteOscontainerdesc(osContainerDescId);
    }

}
