package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2021/1/12-10:40
 */
@SpringBootApplication
public class NsdManagement8004 {
    public static void main(String[] args) {
        SpringApplication.run(NsdManagement8004.class,args);
    }
}
