package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author zhr
 * @date 2021/1/13-14:18
 */
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class ApiGatewayMain8000 {
    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayMain8000.class,args);
    }
}
