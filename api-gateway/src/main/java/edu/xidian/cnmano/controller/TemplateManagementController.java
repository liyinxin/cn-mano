package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author zhr
 * @date 2021/1/13-14:16
 */
@Controller
public class TemplateManagementController {
    @RequestMapping("/template")
    public String listTemplate(Model model){
        model.addAttribute("templates", RestTemplateUtil.doListTemplate());
        return "TemplateManagement/template";
    }
}
