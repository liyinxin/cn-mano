package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author zhr
 * @date 2021/1/13-14:16
 */
@Controller
public class PluginManagementController {
    @RequestMapping("/plugins")
    public String listPlugins(Model model){
        model.addAttribute("plugins", RestTemplateUtil.doListPlugins());
        return "PluginManagement/plugins";
    }
}
