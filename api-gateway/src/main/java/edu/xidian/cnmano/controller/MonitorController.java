package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhr
 * @date 2021/1/13-14:15
 */
@Controller
public class MonitorController {
    @RequestMapping("/podInfo")
    public String listPodInfo(Model model){
        model.addAttribute("podInfos",RestTemplateUtil.doListPodInfo());
        return "Monitor/podInfo";
    }

    @RequestMapping("/nodeInfo")
    public String listNodeInfo(Model model){
        model.addAttribute("nodeInfos",RestTemplateUtil.doListNodeInfo());
        return "Monitor/nodeInfo";
    }

    @RequestMapping("/deploymentInfo")
    public String listDeploymentInfos(Model model){
        model.addAttribute("deploymentInfos",RestTemplateUtil.doListDeploymentInfo());
        return "Monitor/deploymentInfo";
    }

    @RequestMapping("/serviceInfo")
    public String listServiceInfos(Model model){
        model.addAttribute("serviceInfos",RestTemplateUtil.doListServiceInfo());
        return "Monitor/serviceInfo";
    }
}
