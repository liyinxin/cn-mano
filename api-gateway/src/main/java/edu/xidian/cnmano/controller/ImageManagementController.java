package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;
import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/13-14:16
 */
@RequestMapping("/images")
@Controller
public class ImageManagementController {

    @Value("${ImageManagementUrl}")
    String ImageManagementUrl;

    @RequestMapping("")
    public String listImage(Model model){
        model.addAttribute("images", doListImage());
        return "ImageManagement/images";
    }

    private List<Swimagedesc> doListImage(){
        return RestTemplateUtil.doListSwimagedesc();
    }
}
