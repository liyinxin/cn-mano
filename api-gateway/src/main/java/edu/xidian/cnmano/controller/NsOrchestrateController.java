package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhr
 * @date 2021/1/15-15:17
 */
@Controller
public class NsOrchestrateController {

    @RequestMapping("/ns_instances")
    public String listNsinstance(Model model){
        model.addAttribute("nsInstances",RestTemplateUtil.doListNsinstance());
        model.addAttribute("vnfInstances",RestTemplateUtil.doListVnfinstance());
        model.addAttribute("nsds",RestTemplateUtil.doListNsd());
        return "NsOrchestrate/nsinstance";
    }

    @RequestMapping("/vnf_instances")
    public String listVnfinstance(Model model){
        model.addAttribute("vnfInstances",RestTemplateUtil.doListVnfinstance());
        model.addAttribute("vnfds",RestTemplateUtil.doListVnfd());
        model.addAttribute("vnfdfs",RestTemplateUtil.doListVnfdf());
        return "NsOrchestrate/vnfinstance";
    }

    @RequestMapping("/k8svalues")
    public String listK8sValues(Model model){
        model.addAttribute("k8svalues",RestTemplateUtil.doListK8svalues());
        return "NsOrchestrate/k8svalues";
    }
}
