package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.utils.RestTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhr
 * @date 2021/1/13-14:15
 */
@Controller
public class NSDManagementController {

    @RequestMapping("/nsd")
    public String listNSD(Model model){
        model.addAttribute("nsds",RestTemplateUtil.doListNsd());
        model.addAttribute("vnfds",RestTemplateUtil.doListVnfd());
        return "NSDManagement/nsd";
    }

    @RequestMapping("/vnfd")
    public String listVnfd(Model model){
        model.addAttribute("vnfds",RestTemplateUtil.doListVnfd());
        model.addAttribute("vdus",RestTemplateUtil.doListVdu());
        model.addAttribute("vcpds",RestTemplateUtil.doListVirtualCpd());
        model.addAttribute("vnfdfs",RestTemplateUtil.doListVnfdf());
        return "NSDManagement/vnfd";
    }

    @RequestMapping("/vdu")
    public String listVdu(Model model){
        model.addAttribute("vdus",RestTemplateUtil.doListVdu());
        model.addAttribute("vducpds",RestTemplateUtil.doListVducpd());
        model.addAttribute("oscontainerdescs",RestTemplateUtil.doListOscontainerdesc());
        return "NSDManagement/vdu";
    }

    @RequestMapping("/vdup")
    public String listVduProfile(Model model){
        model.addAttribute("vdups",RestTemplateUtil.doListVduProfile());
        model.addAttribute("vdus",RestTemplateUtil.doListVdu());
        model.addAttribute("affinitygroups",RestTemplateUtil.doListAffinityorantiaffinitygroup());
        model.addAttribute("affinityrules",RestTemplateUtil.doListLocalaffinityorantiaffinityrule());
        return "NSDManagement/vdup";
    }

    @RequestMapping("/vcpd")
    public String listVirtualCpd(Model model){
        model.addAttribute("vcpds",RestTemplateUtil.doListVirtualCpd());
        model.addAttribute("vdus",RestTemplateUtil.doListVdu());
        return "NSDManagement/vcpd";
    }

    @RequestMapping("/vnfdf")
    public String listVnfdf(Model model){
        model.addAttribute("vnfdfs",RestTemplateUtil.doListVnfdf());
        model.addAttribute("vdups",RestTemplateUtil.doListVduProfile());
        return "NSDManagement/vnfdf";
    }

    @RequestMapping("/vducpd")
    public String listVducpd(Model model){
        model.addAttribute("vducpds",RestTemplateUtil.doListVducpd());
        return "NSDManagement/vducpd";
    }

    @RequestMapping("/oscontainerdesc")
    public String listOscontainerdesc(Model model){
        model.addAttribute("oscontainerdescs",RestTemplateUtil.doListOscontainerdesc());
        model.addAttribute("images",RestTemplateUtil.doListSwimagedesc());
        return "NSDManagement/oscontainerdesc";
    }
}
