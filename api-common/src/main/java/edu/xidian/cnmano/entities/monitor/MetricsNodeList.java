package edu.xidian.cnmano.entities.monitor;

import io.kubernetes.client.openapi.models.V1ListMeta;

import java.util.List;

/**
 * @author zhr
 * @date 2020/10/22-17:23
 */
public class MetricsNodeList {
    private String kind;
	private String apiVersion;
	private V1ListMeta metadata;
	private List<MetricsNode> items;

	public String getKind() {
		return kind;
	}


	public void setKind(String kind) {
		this.kind = kind;
	}


	public String getApiVersion() {
		return apiVersion;
	}


	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public List<MetricsNode> getItems() {
		return items;
	}


	public void setItems(List<MetricsNode> items) {
		this.items = items;
	}


	public V1ListMeta getMetadata() {
		return metadata;
	}


	public void setMetadata(V1ListMeta metadata) {
		this.metadata = metadata;
	}
}
