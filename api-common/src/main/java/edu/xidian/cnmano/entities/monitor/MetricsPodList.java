package edu.xidian.cnmano.entities.monitor;

import io.kubernetes.client.common.KubernetesListObject;
import io.kubernetes.client.openapi.models.V1ListMeta;

import java.util.List;

/**
 * @author zhr
 * @date 2020/10/22-17:24
 */
public class MetricsPodList implements KubernetesListObject {
    private String kind;
	private String apiVersion;
	private V1ListMeta metadata;
	private List<MetricsPod> items;

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public List<MetricsPod> getItems() {
		return items;
	}

	public void setItems(List<MetricsPod> items) {
		this.items = items;
	}

	public V1ListMeta getMetadata() {
		return metadata;
	}

	public void setMetadata(V1ListMeta metadata) {
		this.metadata = metadata;
	}
}
