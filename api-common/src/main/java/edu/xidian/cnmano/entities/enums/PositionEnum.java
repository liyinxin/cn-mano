package edu.xidian.cnmano.entities.enums;

/**
 * @author shada
 */

public enum PositionEnum {
    /*节点在云端*/
    CLOUD(0),
    /*节点在边缘*/
    EDGE(1);

    private int position;

    PositionEnum(int position){
        this.position = position;
    }

    public int getPosition(){
        return this.position;
    }
}
