package edu.xidian.cnmano.entities.nstransform;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhr
 * @date 2021/1/16-15:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VnfInstanceFull {
    private Integer id;
    private String vnfInstanceName;
    private String vnfInstanceDescription;
    private VirtualizednetworkfunctiondescriptorFull vnfd;
    private String vnfProvider;
    private String vnfProductName;
    private String vnfSoftwareVersion;
    private String vnfdVersion;
    private String instantiationState;
    private VirtualizednetworkfunctiondeploymentflavourFull flavour;
    private String vnfState;
}
