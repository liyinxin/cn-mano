package edu.xidian.cnmano.entities.nsdmanagement;

import java.util.ArrayList;
import java.util.List;

public class VirtualcpdExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public VirtualcpdExample() {
        oredCriteria = new ArrayList<>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCpdIdIsNull() {
            addCriterion("cpdId is null");
            return (Criteria) this;
        }

        public Criteria andCpdIdIsNotNull() {
            addCriterion("cpdId is not null");
            return (Criteria) this;
        }

        public Criteria andCpdIdEqualTo(Integer value) {
            addCriterion("cpdId =", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdNotEqualTo(Integer value) {
            addCriterion("cpdId <>", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdGreaterThan(Integer value) {
            addCriterion("cpdId >", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("cpdId >=", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdLessThan(Integer value) {
            addCriterion("cpdId <", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdLessThanOrEqualTo(Integer value) {
            addCriterion("cpdId <=", value, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdIn(List<Integer> values) {
            addCriterion("cpdId in", values, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdNotIn(List<Integer> values) {
            addCriterion("cpdId not in", values, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdBetween(Integer value1, Integer value2) {
            addCriterion("cpdId between", value1, value2, "cpdId");
            return (Criteria) this;
        }

        public Criteria andCpdIdNotBetween(Integer value1, Integer value2) {
            addCriterion("cpdId not between", value1, value2, "cpdId");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolIsNull() {
            addCriterion("layerProtocol is null");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolIsNotNull() {
            addCriterion("layerProtocol is not null");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolEqualTo(String value) {
            addCriterion("layerProtocol =", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolNotEqualTo(String value) {
            addCriterion("layerProtocol <>", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolGreaterThan(String value) {
            addCriterion("layerProtocol >", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolGreaterThanOrEqualTo(String value) {
            addCriterion("layerProtocol >=", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolLessThan(String value) {
            addCriterion("layerProtocol <", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolLessThanOrEqualTo(String value) {
            addCriterion("layerProtocol <=", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolLike(String value) {
            addCriterion("layerProtocol like", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolNotLike(String value) {
            addCriterion("layerProtocol not like", value, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolIn(List<String> values) {
            addCriterion("layerProtocol in", values, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolNotIn(List<String> values) {
            addCriterion("layerProtocol not in", values, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolBetween(String value1, String value2) {
            addCriterion("layerProtocol between", value1, value2, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andLayerProtocolNotBetween(String value1, String value2) {
            addCriterion("layerProtocol not between", value1, value2, "layerProtocol");
            return (Criteria) this;
        }

        public Criteria andVduIsNull() {
            addCriterion("vdu is null");
            return (Criteria) this;
        }

        public Criteria andVduIsNotNull() {
            addCriterion("vdu is not null");
            return (Criteria) this;
        }

        public Criteria andVduEqualTo(String value) {
            addCriterion("vdu =", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotEqualTo(String value) {
            addCriterion("vdu <>", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduGreaterThan(String value) {
            addCriterion("vdu >", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduGreaterThanOrEqualTo(String value) {
            addCriterion("vdu >=", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLessThan(String value) {
            addCriterion("vdu <", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLessThanOrEqualTo(String value) {
            addCriterion("vdu <=", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLike(String value) {
            addCriterion("vdu like", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotLike(String value) {
            addCriterion("vdu not like", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduIn(List<String> values) {
            addCriterion("vdu in", values, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotIn(List<String> values) {
            addCriterion("vdu not in", values, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduBetween(String value1, String value2) {
            addCriterion("vdu between", value1, value2, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotBetween(String value1, String value2) {
            addCriterion("vdu not between", value1, value2, "vdu");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andProtocolIsNull() {
            addCriterion("protocol is null");
            return (Criteria) this;
        }

        public Criteria andProtocolIsNotNull() {
            addCriterion("protocol is not null");
            return (Criteria) this;
        }

        public Criteria andProtocolEqualTo(String value) {
            addCriterion("protocol =", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotEqualTo(String value) {
            addCriterion("protocol <>", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolGreaterThan(String value) {
            addCriterion("protocol >", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolGreaterThanOrEqualTo(String value) {
            addCriterion("protocol >=", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLessThan(String value) {
            addCriterion("protocol <", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLessThanOrEqualTo(String value) {
            addCriterion("protocol <=", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLike(String value) {
            addCriterion("protocol like", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotLike(String value) {
            addCriterion("protocol not like", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolIn(List<String> values) {
            addCriterion("protocol in", values, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotIn(List<String> values) {
            addCriterion("protocol not in", values, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolBetween(String value1, String value2) {
            addCriterion("protocol between", value1, value2, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotBetween(String value1, String value2) {
            addCriterion("protocol not between", value1, value2, "protocol");
            return (Criteria) this;
        }

        public Criteria andPortIsNull() {
            addCriterion("port is null");
            return (Criteria) this;
        }

        public Criteria andPortIsNotNull() {
            addCriterion("port is not null");
            return (Criteria) this;
        }

        public Criteria andPortEqualTo(Integer value) {
            addCriterion("port =", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortNotEqualTo(Integer value) {
            addCriterion("port <>", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortGreaterThan(Integer value) {
            addCriterion("port >", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortGreaterThanOrEqualTo(Integer value) {
            addCriterion("port >=", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortLessThan(Integer value) {
            addCriterion("port <", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortLessThanOrEqualTo(Integer value) {
            addCriterion("port <=", value, "port");
            return (Criteria) this;
        }

        public Criteria andPortIn(List<Integer> values) {
            addCriterion("port in", values, "port");
            return (Criteria) this;
        }

        public Criteria andPortNotIn(List<Integer> values) {
            addCriterion("port not in", values, "port");
            return (Criteria) this;
        }

        public Criteria andPortBetween(Integer value1, Integer value2) {
            addCriterion("port between", value1, value2, "port");
            return (Criteria) this;
        }

        public Criteria andPortNotBetween(Integer value1, Integer value2) {
            addCriterion("port not between", value1, value2, "port");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualCpd
     *
     * @mbg.generated do_not_delete_during_merge Sat Jan 16 16:46:51 CST 2021
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualCpd
     *
     * @mbg.generated Sat Jan 16 16:46:51 CST 2021
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}