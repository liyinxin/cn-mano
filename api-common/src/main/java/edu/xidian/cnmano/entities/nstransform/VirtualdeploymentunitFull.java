package edu.xidian.cnmano.entities.nstransform;

import edu.xidian.cnmano.entities.nsdmanagement.Vducpd;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VirtualdeploymentunitFull {
    private Integer vduId;
    private String name;
    private String description;
    private List<Vducpd> intCpd;
    private List<OscontainerdescFull> osContainerDesc;
}