package edu.xidian.cnmano.entities.nstransform;

import edu.xidian.cnmano.entities.nsdmanagement.Virtualcpd;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VirtualizednetworkfunctiondescriptorFull {
    private Integer vnfdId;
    private String vnfProvider;
    private String vnfProductName;
    private String vnfSoftwareVersion;
    private String vnfdVersion;
    private List<VirtualdeploymentunitFull> vdu;
    private List<Virtualcpd> virtualCpd;
    private Integer deploymentFlavour;

}