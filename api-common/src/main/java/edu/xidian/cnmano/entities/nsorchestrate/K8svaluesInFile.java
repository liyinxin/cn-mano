package edu.xidian.cnmano.entities.nsorchestrate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class K8svaluesInFile {
    private String name;
    private String deploymentDescription;
    private Integer replicas;
    private String bitrateRequirement;
    private String podDescription;
    private String imageUrl;
    private String portName;
    private Integer containerPort;
    private Integer servicePort;
    private String l4protocol;
    private String requestCpu;
    private String requestMemory;
    private String cpuLimit;
    private String memoryLimit;
    private String location;
    private String podAffinity;
}