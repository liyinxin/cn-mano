package edu.xidian.cnmano.entities.monitor;

import io.kubernetes.client.common.KubernetesObject;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import org.joda.time.DateTime;

import java.util.List;

/**
 * @author zhr
 * @date 2020/11/27-19:57
 */
public class MetricsPod implements KubernetesObject {
		private String apiVersion;
		private String kind;
		private V1ObjectMeta metadata;
		private DateTime timestamp;
		private String window;
		private List<MetricsPodContainer> containers;

		public DateTime getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(DateTime timestamp) {
			this.timestamp = timestamp;
		}
		public String getWindow() {
			return window;
		}
		public void setWindow(String window) {
			this.window = window;
		}
		public List<MetricsPodContainer> getContainers() {
			return containers;
		}
		public void setContainers(List<MetricsPodContainer> containers) {
			this.containers = containers;
		}
		public V1ObjectMeta getMetadata() {
			return metadata;
		}
		public void setMetadata(V1ObjectMeta metadata) {
			this.metadata = metadata;
		}
		public String getApiVersion() {
			return this.apiVersion;
		}
		public String getKind() {
			return this.kind;
		}
	}
