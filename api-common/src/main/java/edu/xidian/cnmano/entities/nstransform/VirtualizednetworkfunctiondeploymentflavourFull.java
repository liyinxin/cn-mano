package edu.xidian.cnmano.entities.nstransform;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VirtualizednetworkfunctiondeploymentflavourFull {
    private Integer flavourId;
    private String description;
    private List<VduprofileFull> vduProfile;
}