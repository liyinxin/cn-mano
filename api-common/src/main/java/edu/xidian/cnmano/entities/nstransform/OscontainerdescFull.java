package edu.xidian.cnmano.entities.nstransform;

import edu.xidian.cnmano.entities.nsdmanagement.Swimagedesc;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OscontainerdescFull {
    private Integer osContainerDescId;
    private Integer requestedCpuResources;
    private Integer requestedMemoryResources;
    private Integer cpuResourceLimit;
    private Integer memoryResourceLimit;
    private Swimagedesc swImageDesc;
}