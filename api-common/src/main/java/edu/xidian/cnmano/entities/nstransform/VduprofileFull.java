package edu.xidian.cnmano.entities.nstransform;

import edu.xidian.cnmano.entities.nsdmanagement.Affinityorantiaffinitygroup;
import edu.xidian.cnmano.entities.nsdmanagement.Localaffinityorantiaffinityrule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VduprofileFull {
    private Integer vduProfileId;
    private Integer vduId;
    private Integer minNumberOfInstances;
    private Integer maxNumberOfInstances;
    private List<Affinityorantiaffinitygroup> affinityOrAntiAffinityGroup;
    private List<Localaffinityorantiaffinityrule> localAffinityOrAntiAffinityRule;
}