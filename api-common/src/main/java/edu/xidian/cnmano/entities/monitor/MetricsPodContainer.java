package edu.xidian.cnmano.entities.monitor;

/**
 * @author zhr
 * @date 2020/11/27-19:58
 */
public class MetricsPodContainer{
		private String name;
		private MetricsUsage usage;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public MetricsUsage getUsage() {
			return usage;
		}
		public void setUsage(MetricsUsage usage) {
			this.usage = usage;
		}
	}
