package edu.xidian.cnmano.entities.nsdmanagement;

import java.util.ArrayList;
import java.util.List;

public class VirtualizednetworkfunctiondescriptorExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public VirtualizednetworkfunctiondescriptorExample() {
        oredCriteria = new ArrayList<>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andVnfdIdIsNull() {
            addCriterion("vnfdId is null");
            return (Criteria) this;
        }

        public Criteria andVnfdIdIsNotNull() {
            addCriterion("vnfdId is not null");
            return (Criteria) this;
        }

        public Criteria andVnfdIdEqualTo(Integer value) {
            addCriterion("vnfdId =", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdNotEqualTo(Integer value) {
            addCriterion("vnfdId <>", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdGreaterThan(Integer value) {
            addCriterion("vnfdId >", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("vnfdId >=", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdLessThan(Integer value) {
            addCriterion("vnfdId <", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdLessThanOrEqualTo(Integer value) {
            addCriterion("vnfdId <=", value, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdIn(List<Integer> values) {
            addCriterion("vnfdId in", values, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdNotIn(List<Integer> values) {
            addCriterion("vnfdId not in", values, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdBetween(Integer value1, Integer value2) {
            addCriterion("vnfdId between", value1, value2, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfdIdNotBetween(Integer value1, Integer value2) {
            addCriterion("vnfdId not between", value1, value2, "vnfdId");
            return (Criteria) this;
        }

        public Criteria andVnfProviderIsNull() {
            addCriterion("vnfProvider is null");
            return (Criteria) this;
        }

        public Criteria andVnfProviderIsNotNull() {
            addCriterion("vnfProvider is not null");
            return (Criteria) this;
        }

        public Criteria andVnfProviderEqualTo(String value) {
            addCriterion("vnfProvider =", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderNotEqualTo(String value) {
            addCriterion("vnfProvider <>", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderGreaterThan(String value) {
            addCriterion("vnfProvider >", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderGreaterThanOrEqualTo(String value) {
            addCriterion("vnfProvider >=", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderLessThan(String value) {
            addCriterion("vnfProvider <", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderLessThanOrEqualTo(String value) {
            addCriterion("vnfProvider <=", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderLike(String value) {
            addCriterion("vnfProvider like", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderNotLike(String value) {
            addCriterion("vnfProvider not like", value, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderIn(List<String> values) {
            addCriterion("vnfProvider in", values, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderNotIn(List<String> values) {
            addCriterion("vnfProvider not in", values, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderBetween(String value1, String value2) {
            addCriterion("vnfProvider between", value1, value2, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProviderNotBetween(String value1, String value2) {
            addCriterion("vnfProvider not between", value1, value2, "vnfProvider");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameIsNull() {
            addCriterion("vnfProductName is null");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameIsNotNull() {
            addCriterion("vnfProductName is not null");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameEqualTo(String value) {
            addCriterion("vnfProductName =", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameNotEqualTo(String value) {
            addCriterion("vnfProductName <>", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameGreaterThan(String value) {
            addCriterion("vnfProductName >", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("vnfProductName >=", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameLessThan(String value) {
            addCriterion("vnfProductName <", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameLessThanOrEqualTo(String value) {
            addCriterion("vnfProductName <=", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameLike(String value) {
            addCriterion("vnfProductName like", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameNotLike(String value) {
            addCriterion("vnfProductName not like", value, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameIn(List<String> values) {
            addCriterion("vnfProductName in", values, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameNotIn(List<String> values) {
            addCriterion("vnfProductName not in", values, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameBetween(String value1, String value2) {
            addCriterion("vnfProductName between", value1, value2, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfProductNameNotBetween(String value1, String value2) {
            addCriterion("vnfProductName not between", value1, value2, "vnfProductName");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionIsNull() {
            addCriterion("vnfSoftwareVersion is null");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionIsNotNull() {
            addCriterion("vnfSoftwareVersion is not null");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionEqualTo(String value) {
            addCriterion("vnfSoftwareVersion =", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionNotEqualTo(String value) {
            addCriterion("vnfSoftwareVersion <>", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionGreaterThan(String value) {
            addCriterion("vnfSoftwareVersion >", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionGreaterThanOrEqualTo(String value) {
            addCriterion("vnfSoftwareVersion >=", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionLessThan(String value) {
            addCriterion("vnfSoftwareVersion <", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionLessThanOrEqualTo(String value) {
            addCriterion("vnfSoftwareVersion <=", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionLike(String value) {
            addCriterion("vnfSoftwareVersion like", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionNotLike(String value) {
            addCriterion("vnfSoftwareVersion not like", value, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionIn(List<String> values) {
            addCriterion("vnfSoftwareVersion in", values, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionNotIn(List<String> values) {
            addCriterion("vnfSoftwareVersion not in", values, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionBetween(String value1, String value2) {
            addCriterion("vnfSoftwareVersion between", value1, value2, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfSoftwareVersionNotBetween(String value1, String value2) {
            addCriterion("vnfSoftwareVersion not between", value1, value2, "vnfSoftwareVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionIsNull() {
            addCriterion("vnfdVersion is null");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionIsNotNull() {
            addCriterion("vnfdVersion is not null");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionEqualTo(String value) {
            addCriterion("vnfdVersion =", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionNotEqualTo(String value) {
            addCriterion("vnfdVersion <>", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionGreaterThan(String value) {
            addCriterion("vnfdVersion >", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionGreaterThanOrEqualTo(String value) {
            addCriterion("vnfdVersion >=", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionLessThan(String value) {
            addCriterion("vnfdVersion <", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionLessThanOrEqualTo(String value) {
            addCriterion("vnfdVersion <=", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionLike(String value) {
            addCriterion("vnfdVersion like", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionNotLike(String value) {
            addCriterion("vnfdVersion not like", value, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionIn(List<String> values) {
            addCriterion("vnfdVersion in", values, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionNotIn(List<String> values) {
            addCriterion("vnfdVersion not in", values, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionBetween(String value1, String value2) {
            addCriterion("vnfdVersion between", value1, value2, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVnfdVersionNotBetween(String value1, String value2) {
            addCriterion("vnfdVersion not between", value1, value2, "vnfdVersion");
            return (Criteria) this;
        }

        public Criteria andVduIsNull() {
            addCriterion("vdu is null");
            return (Criteria) this;
        }

        public Criteria andVduIsNotNull() {
            addCriterion("vdu is not null");
            return (Criteria) this;
        }

        public Criteria andVduEqualTo(String value) {
            addCriterion("vdu =", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotEqualTo(String value) {
            addCriterion("vdu <>", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduGreaterThan(String value) {
            addCriterion("vdu >", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduGreaterThanOrEqualTo(String value) {
            addCriterion("vdu >=", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLessThan(String value) {
            addCriterion("vdu <", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLessThanOrEqualTo(String value) {
            addCriterion("vdu <=", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduLike(String value) {
            addCriterion("vdu like", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotLike(String value) {
            addCriterion("vdu not like", value, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduIn(List<String> values) {
            addCriterion("vdu in", values, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotIn(List<String> values) {
            addCriterion("vdu not in", values, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduBetween(String value1, String value2) {
            addCriterion("vdu between", value1, value2, "vdu");
            return (Criteria) this;
        }

        public Criteria andVduNotBetween(String value1, String value2) {
            addCriterion("vdu not between", value1, value2, "vdu");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdIsNull() {
            addCriterion("virtualCpd is null");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdIsNotNull() {
            addCriterion("virtualCpd is not null");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdEqualTo(String value) {
            addCriterion("virtualCpd =", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdNotEqualTo(String value) {
            addCriterion("virtualCpd <>", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdGreaterThan(String value) {
            addCriterion("virtualCpd >", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdGreaterThanOrEqualTo(String value) {
            addCriterion("virtualCpd >=", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdLessThan(String value) {
            addCriterion("virtualCpd <", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdLessThanOrEqualTo(String value) {
            addCriterion("virtualCpd <=", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdLike(String value) {
            addCriterion("virtualCpd like", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdNotLike(String value) {
            addCriterion("virtualCpd not like", value, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdIn(List<String> values) {
            addCriterion("virtualCpd in", values, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdNotIn(List<String> values) {
            addCriterion("virtualCpd not in", values, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdBetween(String value1, String value2) {
            addCriterion("virtualCpd between", value1, value2, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andVirtualCpdNotBetween(String value1, String value2) {
            addCriterion("virtualCpd not between", value1, value2, "virtualCpd");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourIsNull() {
            addCriterion("deploymentFlavour is null");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourIsNotNull() {
            addCriterion("deploymentFlavour is not null");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourEqualTo(Integer value) {
            addCriterion("deploymentFlavour =", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourNotEqualTo(Integer value) {
            addCriterion("deploymentFlavour <>", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourGreaterThan(Integer value) {
            addCriterion("deploymentFlavour >", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourGreaterThanOrEqualTo(Integer value) {
            addCriterion("deploymentFlavour >=", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourLessThan(Integer value) {
            addCriterion("deploymentFlavour <", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourLessThanOrEqualTo(Integer value) {
            addCriterion("deploymentFlavour <=", value, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourIn(List<Integer> values) {
            addCriterion("deploymentFlavour in", values, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourNotIn(List<Integer> values) {
            addCriterion("deploymentFlavour not in", values, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourBetween(Integer value1, Integer value2) {
            addCriterion("deploymentFlavour between", value1, value2, "deploymentFlavour");
            return (Criteria) this;
        }

        public Criteria andDeploymentFlavourNotBetween(Integer value1, Integer value2) {
            addCriterion("deploymentFlavour not between", value1, value2, "deploymentFlavour");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated do_not_delete_during_merge Sat Jan 16 20:49:59 CST 2021
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VirtualizedNetworkFunctionDescriptor
     *
     * @mbg.generated Sat Jan 16 20:49:59 CST 2021
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}