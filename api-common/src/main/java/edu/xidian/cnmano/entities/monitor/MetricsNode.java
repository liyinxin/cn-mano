package edu.xidian.cnmano.entities.monitor;

import io.kubernetes.client.openapi.models.V1ObjectMeta;
import org.joda.time.DateTime;

/**
 * @author zhr
 * @date 2020/11/27-19:59
 */
public class MetricsNode{
		private V1ObjectMeta metadata;
		private DateTime timestamp;
		private String window;
		private MetricsUsage usage;

		public DateTime getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(DateTime timestamp) {
			this.timestamp = timestamp;
		}
		public String getWindow() {
			return window;
		}
		public void setWindow(String window) {
			this.window = window;
		}
		public MetricsUsage getUsage() {
			return usage;
		}
		public void setUsage(MetricsUsage usage) {
			this.usage = usage;
		}
		public V1ObjectMeta getMetadata() {
			return metadata;
		}
		public void setMetadata(V1ObjectMeta metadata) {
			this.metadata = metadata;
		}

	}
