package edu.xidian.cnmano.utils;

import edu.xidian.cnmano.entities.monitor.DeploymentInfo;
import edu.xidian.cnmano.entities.monitor.NodeInfo;
import edu.xidian.cnmano.entities.monitor.PodInfo;
import edu.xidian.cnmano.entities.monitor.ServiceInfo;
import edu.xidian.cnmano.entities.nsdmanagement.*;
import edu.xidian.cnmano.entities.nsorchestrate.K8svalues;
import edu.xidian.cnmano.entities.nsorchestrate.Nsinstance;
import edu.xidian.cnmano.entities.nsorchestrate.Vnfinstance;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondeploymentflavourFull;
import edu.xidian.cnmano.entities.nstransform.VirtualizednetworkfunctiondescriptorFull;
import edu.xidian.cnmano.entities.nstransform.VnfInstanceFull;
import edu.xidian.cnmano.entities.pluginmanagement.Plugins;
import edu.xidian.cnmano.entities.templatemanagement.Template;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**对spring resttemplate的封装，用于微服务之间发起http请求，获取各种资源
 * @author zhr
 * @date 2021/1/15-9:55
 */
public class RestTemplateUtil {

    private static RestTemplate restTemplate = new RestTemplate();
    
    private static String nsdManagementUrl = "http://192.168.10.139:8004";
    
    private static String imageManagementUrl = "http://192.168.10.139:8005";
    
    private static String NsOrchestrateUrl = "http://192.168.10.139:8002";

    private static String NsdTransformUrl = "http://192.168.10.139:8006";

    private static String monitorUrl = "http://192.168.10.139:8001";

    private static String templateManagementUrl = "http://192.168.10.139:8003";

    private static String pluginManagementUrl = "http://192.168.10.139:8007";

    static <M> List<M> judgeAndReturn(M[] array){
        if(array!=null){
            return Arrays.asList(array);
        }else{
            return new ArrayList<>();
        }
    }
    
    public static List<Networkservicedescriptor> doListNsd(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/nsd", Networkservicedescriptor[].class));
    }

    public static Networkservicedescriptor doGetNsdById(Integer nsdId){
        return restTemplate.getForObject(nsdManagementUrl + "/nsd/"+nsdId,Networkservicedescriptor.class);
    }

    public static List<Virtualdeploymentunit> doListVdu() {
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vdu", Virtualdeploymentunit[].class));
    }

    public static List<Virtualizednetworkfunctiondescriptor> doListVnfd(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vnfd", Virtualizednetworkfunctiondescriptor[].class));
    }

    public static Virtualizednetworkfunctiondescriptor doGetVnfdById(Integer vnfdId){
        return restTemplate.getForObject(nsdManagementUrl + "/vnfd/"+vnfdId,Virtualizednetworkfunctiondescriptor.class);
    }

    public static VirtualizednetworkfunctiondescriptorFull doGetVnfdFullById(Integer vnfdId){
        return restTemplate.getForObject(nsdManagementUrl + "/vnfd/"+vnfdId+"/full",VirtualizednetworkfunctiondescriptorFull.class);
    }

    public static Oscontainerdesc doGetOscontainerdescByVnfdId(Integer vnfdId){
        return restTemplate.getForObject(nsdManagementUrl+"/vnfd/" + vnfdId + "/getOscontainerdesc",Oscontainerdesc.class);
    }

    public static Vduprofile doGetVduProfileByVnfdId(Integer vnfdId) {
        return restTemplate.getForObject(nsdManagementUrl+"/vnfd/" + vnfdId + "/getVduprofile",Vduprofile.class);
    }

    public static List<Vduprofile> doListVduProfile(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vdup", Vduprofile[].class));
    }

    public static List<Virtualcpd> doListVirtualCpd(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vcpd", Virtualcpd[].class));
    }

    public static List<Virtualizednetworkfunctiondeploymentflavour> doListVnfdf(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vnfdf", Virtualizednetworkfunctiondeploymentflavour[].class));
    }

    public static VirtualizednetworkfunctiondeploymentflavourFull doGetVnfdfFullById(Integer flavourId) {
        return restTemplate.getForObject(nsdManagementUrl + "/vnfdf/"+flavourId+"/full", VirtualizednetworkfunctiondeploymentflavourFull.class);
    }

    public static List<Vducpd> doListVducpd(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/vducpd", Vducpd[].class));
    }

    public static List<Oscontainerdesc> doListOscontainerdesc(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/oscontainerdesc", Oscontainerdesc[].class));
    }

    public static List<Affinityorantiaffinitygroup> doListAffinityorantiaffinitygroup(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/affinitygroup", Affinityorantiaffinitygroup[].class));
    }

    public static List<Localaffinityorantiaffinityrule> doListLocalaffinityorantiaffinityrule(){
        return judgeAndReturn(restTemplate.getForObject(nsdManagementUrl + "/affinityrule", Localaffinityorantiaffinityrule[].class));
    }

    public static List<Swimagedesc> doListSwimagedesc(){
        return judgeAndReturn(restTemplate.getForObject(imageManagementUrl+"/images", Swimagedesc[].class));
    }

    public static Swimagedesc doGetSwimageById(Integer id){
        return restTemplate.getForObject(imageManagementUrl+"/images/"+id, Swimagedesc.class);
    }

    public static List<Nsinstance> doListNsinstance(){
        return judgeAndReturn(restTemplate.getForObject(NsOrchestrateUrl+"/ns_instances", Nsinstance[].class));
    }

    public static List<Vnfinstance> doListVnfinstance(){
        return judgeAndReturn(restTemplate.getForObject(NsOrchestrateUrl+"/vnf_instances", Vnfinstance[].class));
    }

    public static K8svalues doTransVnfInstanceToK8SValues(VnfInstanceFull vnfInstanceFull){
        return restTemplate.postForObject(NsdTransformUrl+"/trans_vnfInstance_tok8sValue",vnfInstanceFull, K8svalues.class);
    }

    public static K8svalues doTransOscontainerDescToK8SValues(Oscontainerdesc oscontainerdesc){
        return restTemplate.postForObject(NsdTransformUrl+"/trans_oscontainerdesc_tok8sValue",oscontainerdesc, K8svalues.class);
    }

    public static List<PodInfo> doListPodInfo() {
        return judgeAndReturn(restTemplate.getForObject(monitorUrl+"/podInfo", PodInfo[].class));
    }

    public static List<DeploymentInfo> doListDeploymentInfo() {
        return judgeAndReturn(restTemplate.getForObject(monitorUrl+"/deploymentInfo", DeploymentInfo[].class));
    }

    public static List<NodeInfo> doListNodeInfo() {
        return judgeAndReturn(restTemplate.getForObject(monitorUrl+"/nodeInfo", NodeInfo[].class));
    }

    public static List<ServiceInfo> doListServiceInfo() {
        return judgeAndReturn(restTemplate.getForObject(monitorUrl+"/serviceInfo", ServiceInfo[].class));
    }

    public static List<Template> doListTemplate() {
        return judgeAndReturn(restTemplate.getForObject(templateManagementUrl+"/template", Template[].class));
    }

    public static List<K8svalues> doListK8svalues() {
        return judgeAndReturn(restTemplate.getForObject(NsOrchestrateUrl+"/k8svalues", K8svalues[].class));
    }

    public static List<Plugins> doListPlugins(){
        return judgeAndReturn(restTemplate.getForObject(pluginManagementUrl+"/plugins", Plugins[].class));
    }


}
