package edu.xidian.cnmano.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @author zhr
 * @date 2021/1/21-20:31
 */
@Slf4j
public class SystemCallUtil {
    private static Runtime runtime = Runtime.getRuntime();

    public static void exec(String command){
        try {
            Process process = runtime.exec(command);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8));
            String lineMes;
            while ((lineMes = br.readLine()) != null){
                log.info(lineMes);// 打印输出信息
            }
            //检查命令是否执行失败。
            if (process.waitFor() != 0) {
                if (process.exitValue() == 1)//0表示正常结束，1：非正常结束
                    log.error("命令执行失败!");
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
