package edu.xidian.cnmano.api;

import edu.xidian.cnmano.entities.monitor.MetricsNodeList;
import edu.xidian.cnmano.entities.monitor.MetricsPodList;
import io.kubernetes.client.openapi.*;
import okhttp3.Call;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhr
 * @date 2020/10/22-17:22
 */
public class MetricsServerApi {
    private ApiClient localVarApiClient;

	public final static String BASE_URL="/apis/metrics.k8s.io/v1beta1";

	private String url_get_nodes="/nodes";

	private String url_get_namespace_pods = "/namespaces/{namespace}/pods";

	private String url_get_allNamespce_pods = "/pods";

	public MetricsServerApi() {
		this(Configuration.getDefaultApiClient());
	}

	public MetricsServerApi(ApiClient apiClient) {
		this.localVarApiClient = apiClient;
	}

	public ApiClient getApiClient() {
        return localVarApiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    /**
     *   获取集群各个node节点的cpu和内存用量
     */
    public MetricsNodeList listMetricsNode() throws ApiException {
		String path = BASE_URL+url_get_nodes;
		Call call = buildGetSimpleCall(path, "GET");
		ApiResponse<MetricsNodeList> r = localVarApiClient.execute(call,MetricsNodeList.class);
		return r.getData();
	}

	public Call listMetricsNodeCall() throws ApiException {
		String path = BASE_URL+url_get_nodes;
		return buildGetSimpleCall(path, "GET");
	}

    /**
     * 获取namespace下pods的cpu和内存用量
     * @param namespace
     * @return
     * @throws IOException
     * @throws ApiException
     */
    public MetricsPodList listNamespacedMetricsPod(String namespace) throws ApiException {
    	String path = new String(url_get_namespace_pods);
    	path=path.replaceAll("\\{namespace\\}", namespace);
    	path = BASE_URL+path;
    	Call call = buildGetSimpleCall(path, "GET");
    	ApiResponse<MetricsPodList> r = localVarApiClient.execute(call,MetricsPodList.class);
    	return r.getData();
    }

    public MetricsPodList listPodForAllNamespaces() throws ApiException{
		String path = new String(url_get_allNamespce_pods);
		path = BASE_URL+path;
		Call call = buildGetSimpleCall(path, "GET");
		ApiResponse<MetricsPodList> r = localVarApiClient.execute(call,MetricsPodList.class);
		return r.getData();
	}

    public Call listNamespacedMetricsPodCall(String namespace) throws ApiException {
    	String path = new String(url_get_namespace_pods);
    	path=path.replaceAll("\\{namespace\\}", namespace);
    	path = BASE_URL+path;
    	return buildGetSimpleCall(path, "GET");
    }

    /**
     * 构建简单的Call
     */
    private Call buildGetSimpleCall(String path, String method) throws ApiException {
    	String[] localVarAuthNames = new String[] { "BearerToken" };
    	List<Pair> localVarQueryParams = new ArrayList<Pair>();
    	List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

    	Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    	Map<String, String> localVarCookieParams = new HashMap<String, String>();
    	Map<String, Object> localVarFormParams = new HashMap<String, Object>();
    	final String[] localVarAccepts = {
    			"*/*"
    	};
    	final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
    	if (localVarAccept != null) {
    		localVarHeaderParams.put("Accept", localVarAccept);
    	}

    	final String[] localVarContentTypes = {
    	};
    	final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
    	localVarHeaderParams.put("Content-Type", localVarContentType);

    	return localVarApiClient.buildCall(path, method, localVarQueryParams, localVarCollectionQueryParams, null, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, null);
    }
}
