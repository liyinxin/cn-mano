package edu.xidian.cnmano.requestbody;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhr
 * @date 2021/1/18-9:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScaleNsRequest {
    private Integer nsInstanceId;
    private String scaleType;
    private String scaleVnfData;
}
