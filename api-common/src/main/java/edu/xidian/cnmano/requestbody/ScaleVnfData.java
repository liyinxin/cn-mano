package edu.xidian.cnmano.requestbody;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhr
 * @date 2021/1/18-9:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScaleVnfData {
    private Integer vnfInstanceId;
    private String scaleVnfType;
    private Integer vnfcTargetNumber;
}
