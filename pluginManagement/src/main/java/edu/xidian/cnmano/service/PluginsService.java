package edu.xidian.cnmano.service;

import edu.xidian.cnmano.entities.pluginmanagement.Plugins;

import java.util.List;

/**
 * @author zhr
 * @date 2021/1/21-21:42
 */
public interface PluginsService {
    List<Plugins> listPlugins();

    int createPlugin(Plugins plugin);

    int deletePlugin(Integer id);
}
