package edu.xidian.cnmano.service.impl;

import edu.xidian.cnmano.dao.PluginsMapper;
import edu.xidian.cnmano.entities.pluginmanagement.Plugins;
import edu.xidian.cnmano.entities.pluginmanagement.PluginsExample;
import edu.xidian.cnmano.service.PluginsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/21-21:43
 */
@Service
public class PluginsServiceImpl implements PluginsService {

    @Resource
    private PluginsMapper pluginsMapper;

    @Override
    public List<Plugins> listPlugins() {
        PluginsExample example = new PluginsExample();
        return pluginsMapper.selectByExample(example);
    }

    @Override
    public int createPlugin(Plugins plugin) {
        //这俩参数前端没有提供，这里补上
        plugin.setInstalled("INSTALLED");
        plugin.setEffective("false");
        return pluginsMapper.insert(plugin);
    }

    @Override
    public int deletePlugin(Integer id) {
        return pluginsMapper.deleteByPrimaryKey(id);
    }
}
