package edu.xidian.cnmano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhr
 * @date 2021/1/21-21:38
 */
@SpringBootApplication
public class PluginManagementMain8007 {
    public static void main(String[] args) {
        SpringApplication.run(PluginManagementMain8007.class,args);
    }
}
