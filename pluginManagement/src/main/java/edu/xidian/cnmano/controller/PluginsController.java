package edu.xidian.cnmano.controller;

import edu.xidian.cnmano.entities.pluginmanagement.Plugins;
import edu.xidian.cnmano.service.PluginsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhr
 * @date 2021/1/21-21:42
 */
@RestController
@RequestMapping("/plugins")
public class PluginsController {

    @Resource
    private PluginsService pluginsService;

    @RequestMapping("")
    public List<Plugins> listPlugins(){
        return pluginsService.listPlugins();
    }

    @RequestMapping("/create")
    public int createPlugin(Plugins plugin){
        return pluginsService.createPlugin(plugin);
    }

    @RequestMapping("/delete")
    public int deletePlugin(@RequestParam("id") Integer id){
        return pluginsService.deletePlugin(id);
    }
}
